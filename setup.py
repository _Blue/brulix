from setuptools import setup, find_packages

setup(
    name="brulix",
    version="1",
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        "console_scripts": ["brulix = brulix:main"]
        },
    install_requires=[
        'allora==1.0',
        'gpsd-py3==0.3.0',
        'click==8.1.3',
        'geopy==2.2.0',
        'pynmea2==1.18.0',
        'psutil==5.5.1',
        'Flask==2.1.1',
        'dataclasses-json==0.5.7',
        'werkzeug==2.0.3', # Required for shutdown() method in request
        'PyYAML==6.0',
        'requests==2.26.0',
        'websocket-client==0.57.0',
        'websocket-server==0.6.4',
        'SQLAlchemy==1.4.37',
        'rpi-lcd==0.0.3',
        ],
    dependency_links=[
        'git+ssh://bitbucket.org:_Blue/allora.git#egg=allora-1.0'
    ]
    )
