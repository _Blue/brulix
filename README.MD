# Brulix

Brulix is a client application running on top of the [allora](https://bitbucket.org/_Blue/allora) platform.

Features:

- GPS location broadcast
- Ordered and acknowledged chat messages
- [TODO] distance based LED control

# Installation

```

# First install allora

$ git clone https://bitbucket.org/_Blue/allora
$ pip install allora

# Then clone and install brulix

$ git clone https://bitbucket.org/_Blue/brulix
$ pip install brulix

# Configuration

The configuration file's default location is: `/etc/brulix/config.yml`. See `config.yml.example` for a sample.


# Usage

`brulix [--config /path/to/config] [--debug] [--emulated] [--log-packets] [--local]  <command>`

Parameters:

* `--config` : Path to the configuration file (overrides the `/etc/brulix/config.yaml` default)
* `--debug` : Catch fatal exceptions and open a pdb prompt when they happen
* `--log-packet` : Log packets on stdout
* `--local` : Run the command on this process instead of doing it via RPC to the brulix service

Commands:

* `serve` : Run forever and listen for RPC commands
* `rangetest [--rate rate-seconds]` : Perform a range test and write results on stdout
* `chat <user>` : Open a basic chat session with <user> (bypasses xocial)
* `writeposition <device> <longitude> <latitude> [--heart-state state]` : Write a position entry in the position database
* `scan [--start freq] [--end freq] [--duration]` : Scan and show packets statistics on the specified range
* `importdb </path/to/db.sqlite3>` : Import a location database and write its content in ours
* `debug` : Open a pdb shell with an constructed transport
* `bpm <bpm>` : Force the heart's bpm value
* `listen` : Listen for packets forever and print them on stdout

## Chat

Brulix requires a xocial endpoint to store chat messages. See [xocial](https://bitbucket.org/_Blue/xocial), `xocial.api_endpoint` and `xocial.websocket_endpoint`.

Once configured, brulix will automatically create the xocial source, conversations and contacts.


# GPIO pinout


Pin numbers matching [Raspberry pi pinout](https://pinout.xyz)


```
2 -> LoRa VIN
3 -> LCD SDA
4 -> LCD VIN
5 -> LCD SCL
8 -> Heart connector TX
9 -> LCD GND
10 -> Heart connector RX
6 -> Heart connector GND
14 -> LCD GRND
19 -> LoRa MOSI
21 -> LoRa MISO
22 -> LoRa RST
23 -> LoRa SCK
25 -> Heart connector GND (2)
26 -> LoRa CS
31 -> LoRa RST
```
