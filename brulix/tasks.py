import click
import time
import sys
import geopy.distance
from datetime import datetime
from typing import Dict
from allora import Transport, Packet, OrderedChannel
from .location import LocationBroadcast, LocationReceiver
from .chat import MessagePacket, ChatHandler
from .errors import ConfigurationError


def resolve_user(user_map: Dict[int, str], user: str):
    address = next((e for e in user_map if user_map[e] == user), None)

    if not address:
        raise ConfigurationError(f'No address mapping for user: {user}')

    return address


def listen(transport: Transport, task):
    received = 0
    def on_packet(packet: Packet):
        nonlocal received
        received += 1
        task.output(f'Received packet (tx_power={packet.signal_strength}): {packet}')
        return False # Don't "eat" the packet

    callback = transport.register_packet_callback(on_packet)

    try:
        if task.interactive():
            click.secho(f'Running until ctrl-C', fg='green', bold=True)

        while True:
            task.wait(1)

    except (KeyboardInterrupt, InterruptedError):
        pass
    finally:
        transport.unregister_packet_callback(callback)

    task.output(f'Total packet received: {received}')


def display_transport_statistics(task, transport: Transport):
    task.output('Transport statistics: ')
    task.output(f'- Sent packets: {transport.sent_packets}')
    task.output(f'- Received packets: {transport.received_packets}')
    task.output(f'- Corrupted packets: {transport.corrupted_packets}', color='green' if transport.corrupted_packets == 0 else 'yellow')
    task.output(f'- Device errors: {transport.device_errors}', color='green' if transport.device_errors == 0 else 'red')

def range_test_impl(broadcast: LocationBroadcast, receiver: LocationReceiver,rate: float, gps, task):
    received = []
    device = -1

    def on_location(address: int, timestamp: int, seq_number: int, longitude: float, latitude: float, signal_strength: int, battery_level: int, heart_state: int):
        nonlocal device

        if device == -1:
            device = address
            task.output(f'Bound to device: {device}', color='green', bold=True)
        elif device != address:
            task.output(f'Ignored packet from device: {address}')
            return

        if latitude == 0 and longitude == 0:
            task.output(f'Received packet without location data', color='yellow')

        else:
            current_location = gps.get_position()
            if current_location is None:
                task.output(f'Received location packet: {latitude},{longitude} [ No location available to compare with yet ], signal: {signal_strength}', color='yellow')
            else:
                distance = round(geopy.distance.geodesic((current_location.latitude, current_location.longitude), (latitude, longitude)).m)

                if not received:
                    task.output(f'Received first location packet: distance: {distance}m, signal: {signal_strength}', color='green', bold=True)
                else:
                    task.output(f'Received location: distance: {distance}m, lost packets: {seq_number - received[-1] - 1}, signal: {signal_strength}', color='green')

        received.append(seq_number)

    try:
        with receiver.register_callback(on_location), broadcast.push_poll_time(rate):
            if task.interactive():
                print('Running until ctrl-c')
            else:
                task.output(f'Range test running (rate: {rate})', color='green')

            last_packet = None
            last_packet_ts = None
            while True:
                task.wait(5)
                if last_packet and last_packet == received[-1]:
                    task.output(f'No packet received for {int(time.time() - last_packet_ts)} seconds', color='red', bold=True)
                elif received:
                    last_packet = received[-1]
                    last_packet_ts = time.time()

    except (KeyboardInterrupt, InterruptedError):
        task.output('\nStatistics:', bold=True)
        task.output(f'- Received packets: {len(received)}', color='green')
        if len(received) >= 2:
            task.output(f'- Lost packets: {(received[-1] - received[0]) - (len(received) - 1)}', color='red')

        display_transport_statistics(task, broadcast.channel.transport)


def chat_impl(channel: OrderedChannel, user: str, task):
    prompt = f'Message to {user}: '
    sent = 0
    received = 0
    def on_message(message: MessagePacket):
        nonlocal received
        received += 1

        if task.interactive():
            # Erase prompt line
            sys.stderr.write('\b' * 100)
            sys.stderr.flush()
            click.secho(click.style(f'[{message.timestamp}] ', bold=True) + click.style(user + ': ', bold=True, fg='green') + click.style(message.text, bold=True))
        else:
            task.output(f'[{message.timestamp}] {user}: {message.text}', bold=True)

        if task.interactive():
            # Rewrite prompt:
            sys.stderr.write(prompt)
            sys.stderr.flush()


    task.output(f'Waiting for channel to connect', color='yellow', bold=True)
    while not channel.wait_for_connect(0.1):
        task.wait(0.1)

    task.output(f'Channel connected', color='green', bold=True)

    chat = ChatHandler(channel, on_message)

    try:
        with chat:
            while True:

                if task.interactive():
                    text = click.prompt(prompt, prompt_suffix='', err=True)
                else:
                    text = task.input()

                ts = datetime.now()
                chat.send_message(ts, user, text)
                ts = ts.replace(microsecond=0)
                if not task.interactive():
                    task.output(f'[{ts}] Sent: {text}', bold=False)
                sent += 1
    except (KeyboardInterrupt, click.exceptions.Abort):
        task.output('Statistics: ')
        task.output(f'- Sent messages: {sent}', color='green', bold=True)
        task.output(f'- Received messages: {sent}', color='green', bold=True)

        task.output('Channel statistics: ')
        task.output(f'{channel.stats}')
        display_transport_statistics(task, channel.transport)

