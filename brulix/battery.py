import dataclasses
import time
import logging
import traceback
from collections import deque
from threading import Thread, Event, Lock

logger = logging.getLogger(__name__)

@dataclasses.dataclass
class BatteryRecord:
    timestamp: float
    battery_percentage: int

class BatteryRecording:
    def __init__(self, error_routine, poll_time_seconds: float, battery_capacity_mah: int, consumption_sample_size: int, pijuice):
        if consumption_sample_size < 1:
            raise ValueError("consumption_sample_size must be 1 or higher")

        self.error_routine = error_routine
        self.poll_time_seconds = poll_time_seconds
        self.thread = None
        self.stop_event = None
        self.battery_capacity_mah = battery_capacity_mah
        self.pijuice = pijuice
        self.battery_records = deque(maxlen=consumption_sample_size)
        self.lock = Lock()

    def start(self):
        assert self.thread is None
        assert self.stop_event is None

        self.stop_event = Event()
        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop(self):
        assert self.thread is not None
        assert self.stop_event is not None
        assert not self.stop_event.is_set()

        self.stop_event.set()
        self.thread.join()

        self.stop_event = None
        self.thread = None

    def __enter__(self, *args, **kwargs):
        return self.start(*args, **kwargs)

    def __exit__(self, *args, **kwargs):
        if self.thread is not None:
            self.stop()

    def run(self):
        logger.info('BatteryRecording starting')
        while not self.stop_event.wait(self.poll_time_seconds):
            try:
                with self.lock:
                    self.record(time.time())
                self.log_debug()
            except Exception as e:
                logger.error(f'Error while recording battery status, {traceback.format_exc()}')
                self.error_routine(e)

        logger.info('BatteryRecording stopping')


    def record(self, time):
        charge_level = self.pijuice.status.GetChargeLevel()

        if charge_level['error'] != "NO_ERROR":
            logger.warning(f"Battery charge level error: {charge_level['error']}")
            return

        battery_percentage = int(charge_level['data'])
        logger.debug(f'Logging record at {time} with battery_percentage: {battery_percentage}')

        self.battery_records.appendleft(BatteryRecord(time, battery_percentage))

    """Calculate the duration between pourcentage changes and project the discharge rate from that. Returns the value in minutes. Negative values denote charging."""
    def get_remaining_time(self):
        if len(self.battery_records) < 2:
            return None

        charge_level = self.pijuice.status.GetChargeLevel()

        if charge_level['error'] != "NO_ERROR":
            logger.warning(f"Battery charge level error: {charge_level['error']}")
            return None

        with self.lock:
            percentage_diff = self.battery_records[-1].battery_percentage - self.battery_records[0].battery_percentage

            if percentage_diff == 0:
                return None

            timestamp_delta = (self.battery_records[0].timestamp - self.battery_records[-1].timestamp)

            minutes_per_percent_change = timestamp_delta / percentage_diff / 60

            return round(minutes_per_percent_change * int(charge_level['data']))

    def get_battery_charge_percentage(self):
        charge_level = self.pijuice.status.GetChargeLevel()

        if charge_level['error'] != "NO_ERROR":
            logger.warning(f"Battery charge level error: {charge_level['error']}")
            return None

        return charge_level['data']


    def log_debug(self):
        logger.debug(f'Percentage estimate: {self.get_remaining_time()}')
