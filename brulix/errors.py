class ConfigurationError(RuntimeError):
    pass

class MalformedPacketError(RuntimeError):
    pass

class HTTPException(RuntimeError):
    def __init__(self, code: int, message: str):
        self.code = code
        self.message = message

        super().__init__(message)

class ServerNotRunningError(HTTPException):
    def __init__(self):
        super().__init__(409, 'Server is not running')

class ChannelInUseError(HTTPException):
    def __init__(self, id: int):
        super().__init__(409, f'Channel {id} is in use')
