import traceback
import time
from logging import getLogger
from threading import Thread, Lock, Event
from allora import OrderedChannel, Packet
from datetime import datetime
from .xocial import XocialClient, Source, Contact, Conversation, Message, parse_timestamp, MessageNotFound, XocialWebsocketClient
from enum import Enum
from dataclasses import dataclass
from typing import Optional, List

TEXT_ENCODING = 'utf-8'

logger = getLogger(__name__)


MESSAGE_PRIORITY = 1
ACK_PRIORITY = 0

class ChatPacketType(Enum):
    Invalid = 0
    Message = 1
    Ack = 2

class ChatPacket:
    def __init__(self, content: bytes):
        self.raw_content = content

    def read_int(self, offset: int, size: int) -> int:
        assert offset >= 0
        assert size > 0

        if offset + size > len(self.raw_content):
            raise RuntimeError(f'Attempted to read at offset {offset + size}, but packet size is {len(self.raw_content)}')

        return int.from_bytes(self.raw_content[offset:offset + size], byteorder='big')


class MessagePacket(ChatPacket):
    # Format:
    # struct MessagePacket
    # {
    #   uint64 timestamp;
    #   uint64 sender_id;
    #   uint64? reply_to_id
    #   char[] text
    # }

    def __init__(self, content: bytes):
        if len(content) <= 24:
            raise RuntimeError(f'Unexpected message length for MessagePacket: {len(content)} bytes')

        super().__init__(content)

    @property
    def timestamp(self) -> datetime:
        return datetime.fromtimestamp(self.read_int(1, 8))

    @property
    def sender_id(self) -> int:
        return self.read_int(9, 8)

    @property
    def reply_to_receiver_id(self) -> int: # 0 means empty
        value = self.read_int(17, 8)
        return None if value == 0 else value

    @property
    def text(self) -> str:
        return self.raw_content[25:].decode(TEXT_ENCODING)

    @staticmethod
    def build(timestamp: datetime, text: str, sender_id: int, reply_to_id: int):
        content = (ChatPacketType.Message.value.to_bytes(1, 'big')
                   + round(timestamp.timestamp()).to_bytes(8, 'big')
                   + sender_id.to_bytes(8, 'big')
                   + (reply_to_id or 0).to_bytes(8, 'big')
                   + text.encode(TEXT_ENCODING))

        return MessagePacket(content)

class MessageAckPacket(ChatPacket):
    # Format
    # struct MessageAckPacket
    # {
    #   uint64 sender_id
    #   uint64 receiver_id
    # }

    def __init__(self, content: bytes):
        if len(content) != 17:
            raise RuntimeError(f'Unexpected message length for MessageAckPacket: {len(content)} bytes')

        super().__init__(content)

    @property
    def sender_id(self) -> int:
        return self.read_int(1, 8)

    @property
    def receiver_id(self) -> int:
        return self.read_int(9, 8)

    @staticmethod
    def build(sender_id: int, receiver_id: int) -> MessagePacket:
        content = (ChatPacketType.Ack.value.to_bytes(1, 'big')
                   + sender_id.to_bytes(8, 'big')
                   + receiver_id.to_bytes(8, 'big'))

        return MessageAckPacket(content)


class ChatHandler:
    def __init__(self, channel: OrderedChannel, on_message):
        self.channel = channel
        self.on_message = on_message
        self.running = False
        self.lock = Lock() # Protects against a callback being called after a stop()

    def on_packet(self, packet: Packet):
        logger.info(f'Received packet: {packet}')
        with self.lock:
            self.on_message(MessagePacket(packet.payload))

    def send_message(self, timestamp: datetime, user: str, message: str):
        packet = MessagePacket.build(timestamp, user, message)
        assert datetime.fromtimestamp(round(timestamp.timestamp())) == packet.timestamp
        assert user == packet.user
        assert message == packet.text

        logger.info(f'Sending packet: {packet.raw_content}')

        self.channel.send(packet.raw_content)

    def start(self):
        assert not self.running

        self.channel.set_receive_callback(self.on_packet)

    def stop(self):
        assert self.running
        with self.lock:
            self.channel.set_receive_callback(None)

    def __enter__(self, *args, **kwargs):
        return self.start(*args, **kwargs)

    def __exit__(self, *args, **kwargs):
        if self.running:
            self.stop()

class ChatChannel:
    @dataclass
    class Status:
        conversation_id: int
        destination: int
        transmitted_messages: int
        transmitted_acks: int
        running: bool
        thread_running: bool
        current_transaction: Optional[int]

    def __init__(self, source: Source, conversation: Conversation, client: XocialClient, channel: OrderedChannel, ack_timeout: int, error_routine):
        self.client = client
        self.source = source
        self.conversation = conversation
        self.error_routine = error_routine
        assert len(conversation.members) == 2
        self.me_contact = next(e for e in conversation.members if e.is_me)
        self.contact = next(e for e in conversation.members if not e.is_me)

        self.channel = channel
        self.thread = None
        self.running = False

        self.ack_timeout = ack_timeout
        self.wake_event = Event()
        self.ack_received_event = Event()
        self.queue_complete = Event()
        self.transmitted_messages = 0
        self.transmitted_acks = 0
        self.queue_lock = Lock()
        self.transaction_lock = Lock()
        self.pending_messages = []
        self.pending_acks = []
        self.current_message = None
        self.current_ack_timeout = None

    def start(self):
        assert self.thread is None
        assert not self.running

        logger.info(f'ChatChannel for conversation {self.conversation.id} starting')

        self.channel.set_receive_callback(self.on_packet_received)
        self.channel.start()
        self.running = True
        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop(self):
        assert self.thread is not None
        assert self.running

        logger.info(f'ChatChannel for conversation {self.conversation.id} stopping')

        # Stop the channel before setting running = False
        # So we don't get a receive callback with running = False
        self.channel.set_receive_callback(None)
        self.channel.stop()
        self.running = False

        self.wake_event.set()
        self.thread.join()
        self.thread = None

        logger.info(f'ChatChannel for conversation {self.conversation.id} stopped')

    def __enter__(self, *args, **kwargs):
        self.client.__enter__()
        self.start(*args, **kwargs)

        return self

    def __exit__(self, error_type, error, traceback):
        if error is None:
            self.sync()

        if self.running:
            self.client.__exit__()
            self.stop()

    def sync(self, message_count=None, ack_count=None):
        with self.queue_lock:
            message_wait_index = self.transmitted_messages + (len(self.pending_messages) if message_count is None else message_count)
            ack_wait_index = self.transmitted_acks + (len(self.pending_acks)  if ack_count is None else ack_count)

        while self.transmitted_messages < message_wait_index or self.transmitted_acks < ack_wait_index:
            self.queue_complete.wait()

    def status(self) -> Status:
        return ChatChannel.Status(conversation_id=self.conversation.id,
                                  destination=self.channel.destination,
                                  transmitted_messages=self.transmitted_messages,
                                  transmitted_acks=self.transmitted_acks,
                                  running=self.running,
                                  thread_running=self.thread is not None and self.thread.is_alive(),
                                  current_transaction=self.current_message.id if self.current_message else None)

    def wait_for_transaction(self):
        while self.current_message is None:
            self.queue_complete.wait()

    def on_message_packet_received(self, packet: MessagePacket):
        logger.debug(f'Received message packet for sender_id={packet.sender_id} in conversation for conversation {self.conversation.id}')

        # Check if the message already exists in xocial (send an ack even if it exists, in case the ack was lost)
        try:
            message = self.client.get_message_by_remote_id(self.conversation, packet.sender_id)
            logger.warning(f'Message {message.id} already exists in xocial, sending ack anyway')
        except MessageNotFound:
            message = self.client.create_message(self.conversation, packet.text, packet.sender_id, self.contact.id, time.time(), packet.timestamp.timestamp(), packet.reply_to_receiver_id)
            logger.debug(f'Created local message for sender_id={packet.sender_id} receiver_id={message.id} , timestamp={packet.timestamp.timestamp()} in conversation for conversation {self.conversation.id}')

        self.pending_acks.append(message)
        self.wake_event.set()

    def on_ack_packet_received(self, packet: MessageAckPacket):
        logger.debug(f'Received ack packet for sender_id={packet.sender_id}, receiver_id={packet.receiver_id} in conversation for conversation {self.conversation.id}')

        message = self.client.get_message(packet.receiver_id)
        if message.remote_id is not None:
            logger.warning(f'Message {message.id} is already acknowledged in xocial, dropping')
            return

        message = self.client.update_message(self.conversation, packet.receiver_id, packet.sender_id, time.time(), message.reply_to.id if message.reply_to else None)
        assert message.remote_id == packet.sender_id
        assert message.id == packet.receiver_id


        with self.transaction_lock:
            if self.current_message is None:
                logger.warning(f'Received message ack for {message.id}, but no transction is in progress')
            elif self.current_message.id != message.id:
                logger.warning(f'Received ack packet with message {message.remote_id}, but currently waiting for {self.current_message.id}')
            else:
                logger.debug(f'Updated message {message.id} with ack (remote_id={packet.receiver_id})')
                self.wake_event.set()
                self.current_ack_timeout = None
                self.current_message = None

    def on_packet_received(self, packet: Packet):
        try:
            packet_type = packet.read_payload_int(0, 1)
            if packet_type == ChatPacketType.Message.value:
                self.on_message_packet_received(MessagePacket(packet.payload))
            elif packet_type == ChatPacketType.Ack.value:
                self.on_ack_packet_received(MessageAckPacket(packet.payload))
            else:
                raise RuntimeError(f'Failed to parse ChatPacket: {packet}, unexpected packet_type: {packet_type}')
        except Exception as e:
            logger.error(f'Failed to process chat packet, {traceback.format_exc()}')
            self.error_routine(e)


    def run(self):
        try:
            self.run_impl()
        except Exception as e:
            logger.error(f'ChatChannel for conversation {self.conversation.id} caught unexpected exception, {traceback.format_exc()}')
            self.error_routine(e)

    def run_impl(self):
        while self.running:
            with self.transaction_lock:
                # Make sure that we don't wait longer than the transaction timeout
                wait_time = 5 if self.current_ack_timeout is None else min(5, time.time() - self.current_ack_timeout)

            self.wake_event.wait(wait_time)
            if not self.running:
                break

            self.wake_event.clear() # Clear the wake event for next iteration

            with self.queue_lock:
                while self.pending_acks and self.running:
                    try:
                        self.transmit_ack(self.pending_acks.pop(0))
                    except Exception as e:
                        logger.error(f'Failed to transmit ack, {traceback.format_exc()}')
                        self.error_routine(e)
                    finally:
                        self.transmitted_acks += 1
                        self.queue_complete.set()
                        self.queue_complete.clear()

                with self.transaction_lock:
                    message_to_repeat = None
                    transaction_in_progress = False

                    if self.current_message is not None:
                        transaction_in_progress = True

                        if time.time() > self.current_ack_timeout:
                            logger.warning(f'Ack for message {self.current_message.id} timed out, repeating message')
                            message_to_repeat = self.current_message

                # This must not be done under the transaction lock (since the receive_packet method also holds it, and blocking the channel thread would deadlock)
                if message_to_repeat is not None:
                    try:
                        # It's theoretically possible that the ack has just been received in another thread since we dropped the lock.
                        # In that case repeating the transaction and setting current_ack_timeout is harmless because we use self.current_message
                        # to check wether a transaction is in progress or not
                        self.transmit_message(message_to_repeat)
                    except Exception as e:
                        logger.error(f'Failed to transmit message, {traceback.format_exc()}')
                        self.error_routine(e)
                    finally:
                        self.current_ack_timeout = time.time() + self.ack_timeout # Set this here so that failed transactions are retried as well

                # Don't transmit a new message until the previous one is acked
                if not transaction_in_progress and len(self.pending_messages) > 0:
                    try:
                        message = self.pending_messages.pop(0)
                        self.current_message = message
                        self.transmit_message(message)
                    except Exception as e:
                        logger.error(f'Failed to transmit message, {traceback.format_exc()}')
                        self.error_routine(e)
                    finally:
                        self.transmitted_messages += 1
                        self.queue_complete.set()
                        self.queue_complete.clear()
                        self.current_ack_timeout = time.time() + self.ack_timeout # Set this here so that failed transactions are retried as well

    def push_message(self, message: Message):
        assert message.conversation.id == self.conversation.id

        with self.transaction_lock:
            if any(e.id == message.id for e in self.pending_messages) or (self.current_message is not None and self.current_message.id == message.id):
                logger.debug(f'Message {message.id} already in queue for ChatChannel for conversation {self.conversation.id} , skipping')
                return

        logger.debug(f'Adding message {message.id} to queue')
        self.pending_messages = sorted([e for e in self.pending_messages + [message]], key = lambda e: e.id)
        self.wake_event.set()

    def transmit_ack(self, message: Message):
        assert message.remote_id is not None

        logger.debug(f'Transmitting ack for message {message.id} in conversation for conversation {self.conversation.id}')
        packet = MessageAckPacket.build(message.id, message.remote_id)
        self.channel.send(packet.raw_content)

    def transmit_message(self, message: Message):
        logger.debug(f'Transmitting message {message.id} in conversation for conversation {self.conversation.id}')
        packet = MessagePacket.build(parse_timestamp(message.timestamp), message.text, message.id, message.reply_to.remote_id if message.reply_to else None)
        self.channel.send(packet.raw_content)


class ChatService:

    class State(Enum):
        CREATED = 1
        INTIALIZING = 2
        RUNNING = 3
        STOPPED = 4
        FAILED = 5


    @dataclass
    class Status:
        state: str
        running: bool
        message_poll_count: int
        source_name: str
        channels: List[ChatChannel.Status]

    def __init__(self,
                 error_routine,
                 client: XocialClient,
                 websocket_endpoint,
                 source_name: str,
                 user_map: dict,
                 address: int,
                 message_lookup_rate: int,
                 ack_timeout: float,
                 create_channel):

        self.error_routine = error_routine
        self.client = client
        self.source_name = source_name
        self.thread = None
        self.user_map = user_map
        self.address = address
        self.state = ChatService.State.CREATED
        self.message_lookup_rate = message_lookup_rate
        self.interrupt_event = Event()
        self.initialized_event = Event()
        self.sync_event = Event()
        self.ack_timeout = ack_timeout
        self.create_channel = create_channel
        self.websocket_client = XocialWebsocketClient(websocket_endpoint, ['Message'], self.on_message_updated)
        self.message_poll_count = 0
        self.message_poll_lock = Lock()
        self.running = False
        self.chat_channels = []

    def status(self) -> Status:
        return ChatService.Status(state=self.state.name,
                                  running=self.running,
                                  message_poll_count=self.message_poll_count,
                                  source_name=self.source_name,
                                  channels=None if not self.initialized_event.is_set() else [e.status() for e in self.chat_channels])

    def transition(self, new_state: State):
        logger.info(f'ChatService transitioning from {self.state} to {new_state}')

        self.state = new_state

    def get_or_create_contact(self, name: str) -> Contact:
        source_contact = self.client.get_or_create_source_contact(self.source, name, False)
        return self.client.get_or_create_contact(self.source, source_contact)

    def initialize(self, create_channel):
        self.source = self.client.get_or_create_source(self.source_name)
        logger.info(f'Acquired xocial source: {self.source.id}')

        me_name = self.user_map[self.address]
        me_source_contact = self.client.get_or_create_source_contact(self.source, me_name, is_me=True)
        self.me_contact = self.client.get_or_create_contact(self.source, me_source_contact)
        logger.info(f'Acquired "me" contact: {self.me_contact.id}')

        self.contacts = {e: self.get_or_create_contact(self.user_map[e]) for e in self.user_map if e != self.address}
        logger.info(f'Acquired {len(self.contacts)} contacts from xocial')

        self.conversations = {e: self.client.get_or_create_conversation(self.source, self.user_map[e], [self.me_contact, self.contacts[e]]) for e in self.user_map if e != self.address}
        logger.info(f'Acquired {len(self.contacts)} conversations from xocial')

        self.chat_channels = [ChatChannel(self.source, self.conversations[e], self.client, create_channel(e), self.ack_timeout, self.error_routine) for e in self.user_map if e != self.address]
        logger.info(f'Created {len(self.chat_channels)} chat channels')

    def on_message_updated(self, message: Message):
        if message.remote_id is None and message.conversation.source.id == self.source.id:
            logger.debug(f'Message {message.id} discovered via websocket')
            self.dispatch_message(message)

    def sync(self):
        self.initialized_event.wait()

        with self.message_poll_lock:
            poll_count = self.message_poll_count

        while self.message_poll_count <= poll_count:
            self.interrupt_event.set()
            self.sync_event.wait()

        for e in self.chat_channels:
            e.sync()

    def run_impl(self, create_channel):
        logger.info(f'ChatService starting')

        self.transition(ChatService.State.INTIALIZING)
        self.initialize(create_channel)

        with self.websocket_client:

            while self.running and not self.websocket_client.wait_for_connection(self.interrupt_event):
                logger.warning('Retrying to wait for websocket connection...')

            self.look_for_unsent_messages() # Needs to be done first to make sure messages are transmitted in order

            for e in self.chat_channels:
                e.start()

            self.transition(ChatService.State.RUNNING)
            self.initialized_event.set()

            while self.running:
                self.interrupt_event.wait(self.message_lookup_rate)
                self.interrupt_event.clear()

                if not self.running:
                    break

                with self.message_poll_lock:
                    self.look_for_unsent_messages()
                    self.message_poll_count += 1
                    self.sync_event.set()
                    self.sync_event.clear()

    def run(self):
        while self.running: # Retry given that xocial might not be running yet
            channels = []
            def register_channel(destination):
                channel = self.create_channel(destination)
                channels.append(channel)

                return channel.channel

            try:
                self.run_impl(register_channel)
            except Exception as e:
                logger.error(f'Caught unexpected exception in ChatService, {traceback.format_exc()}')
                self.error_routine(e)
                time.sleep(1)
            finally:
                for e in channels:
                    try:
                        e.unregister()
                    except:
                        logger.error(f'Error while unregistering channel, {traceback.format_exc()}')

        self.transition(ChatService.State.STOPPED)

        for e in self.chat_channels:
            try:
                e.stop()
            except Exception as e:
                self.error_routine(e)
                logger.error(f'Caught unexpected exception while stopping ChatChannel, {traceback.format_exc()}')


    def get_conversation_target(self, conversation: Conversation) -> Contact:
        logger.verbose('Looking up message in conversation: {conversation.id}')
        target = next((e for e in conversation.members if not e.is_me), None)
        if target is None:
            raise RuntimeError(f'Target not found in conversation: {conversation}')

        return target

    def look_for_unsent_messages(self):
        logger.debug('Starting xocial message poll')

        messages = self.client.get_unsent_messages(self.source)
        for e in messages:
            if e.sent_timestamp is None:
                logger.debug(f'Message {e.id} is unsent, queuing for transmission')
                self.dispatch_message(e)

    def dispatch_message(self, message: Message):
        for e in self.chat_channels:
            if e.conversation.id == message.conversation.id:
                e.push_message(message)
                return

        logger.error(f'No conversation found for message {message.id} (conversation: {message.conversation.id})')

    def get_channel(self, conversation: Conversation) -> ChatChannel:
        assert self.initialized_event.is_set()

        return next(e for e in self.chat_channels if e.conversation.id == conversation.id)

    def start(self):
        assert self.thread is None
        assert not self.running

        self.running = True
        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop(self):
        logger.info('ChatService Stopping')
        self.interrupt_event.set()
        if self.running:
            self.running = False
            self.interrupt_event.set()
            self.thread.join()
            self.thread = None

            logger.info('ChatService stopped')

    def __enter__(self, *args, **kwargs):
        self.start()
        return self

    def __exit__(self, *args, **kwargs):
        if self.running:
            self.sync()
            self.stop()
