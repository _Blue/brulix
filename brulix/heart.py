import geopy.distance
import time
import traceback
import random
from math import exp
from datetime import datetime
from logging import getLogger
from threading import Thread, Event, Lock
from .location import LocationReceiver, LocationBroadcast
from .battery import BatteryRecording
from .errors import ConfigurationError
from enum import Enum
from serial import Serial

logger = getLogger(__name__)


class State(Enum):
    INVALID = 0
    CREATED = 1
    RUNNING = 2
    CONNECTING = 3
    CONNECTED = 4

class Heart:
    def __init__(
            self,
            location_receiver: LocationReceiver,
            location_broadcast: LocationBroadcast,
            address: int,
            target: int,
            rate: float,
            display_distance: bool,
            d: float,
            k: float,
            win_messages: list,
            win_message_timeout: float,
            connecting_message_timeout: float,
            serial: str,
            serial_speed: int,
            serial_write_rate: float,
            serial_timeout: float,
            vibration_pin: int,
            duty_cycle_ratio: float,
            vibration_bpm_biais: float,
            messages: list,
            lcd,
            gps,
            battery: BatteryRecording):

        self.location_receiver = location_receiver
        self.location_broadcast = location_broadcast
        self.address = address
        self.target = target
        self.rate = rate
        self.gps = gps
        self.interrupt_event = Event()
        self.thread = None
        self.connector_thread = None
        self.lcd = lcd
        self.display_distance = display_distance
        self.battery = battery
        self.error_flag = False
        self.d = d
        self.k = k
        self.serial_path = serial
        self.serial_speed = serial_speed
        self.serial_write_rate = serial_write_rate
        self.serial_timeout = serial_timeout
        self.vibration_pin = vibration_pin
        self.duty_cycle_ratio = duty_cycle_ratio
        self.vibration_bpm_biais = vibration_bpm_biais
        self.connecting_message_timeout = connecting_message_timeout
        self.win_message_timeout = win_message_timeout
        self.win_messages = win_messages
        self.running = False
        self.last_seq_number = None
        self.forced_bpm = None
        self.messages = messages

        if any(len(e) > 16 for e in self.messages):
            raise ConfigurationError('heart.messages entry is over the limit (16 chars)')

        if self.vibration_pin is not None:
            from RPi import GPIO
            GPIO.setup(self.vibration_pin, GPIO.OUT)
            self.pwm = GPIO.PWM(self.vibration_pin, 1)
        else:
            self.pwm = None

        self.state = None
        self.transition(State.CREATED)

    def start(self):
        assert self.thread is None
        assert self.connector_thread is None
        assert not self.running

        self.running = True
        if self.pwm is not None:
            self.pwm.start(0)

        self.thread = Thread(target=self.run)
        self.thread.start()

        self.connector_thread = Thread(target=self.run_connector)
        self.connector_thread.start()

    def stop(self):
        assert self.thread is not None
        assert self.connector_thread is not None
        assert self.running

        self.running = False
        self.interrupt_event.set()
        self.thread.join()
        self.connector_thread.join()

        self.thread = None
        self.connector_thread = None
        self.interrupt_event.clear()

        if self.pwm is not None:
            self.pwm.stop()

    def output(self, text: str, line=1):
        warn = self.state == State.RUNNING and (self.error_flag or self.gps.backoff)

        content = f'{"!" if warn and line == 1 else ""}{text}'
        if self.lcd is not None:
            self.lcd.text(content, line)
        else:
            logger.info(f'Heart output on line {line}: {text}')

    def run(self):
        self.error_flag = False

        self.transition(State.RUNNING)

        while self.running:
            try:
                self.run_impl()
            except:
                self.error_flag = True
                logger.error(f'Caught unexpected exception in Heart, {traceback.format_exc()}')
                time.sleep(1)

    def compute_bpm(self, distance: float) -> int:
        # Equation: d - (d / (1 + e ^ -kx))

        return round(self.d - (self.d / (1 + exp(-self.k * distance))), 1)

    def show_connecting_message(self):
        logger.info('Displaying connecting animation')
        self.interrupt_event.clear() # Needed to prevent fast looping if the event happens to be set here

        while self.state == State.CONNECTING and self.running:
            for e in range(0, 5):
                self.output('<3 ' * e)
                self.interrupt_event.wait(self.connecting_message_timeout)
                self.interrupt_event.clear()


    def show_connected_message(self):
        logger.info('Displaying win animation')
        self.interrupt_event.clear() # Needed to prevent fast looping if the event happens to be set here

        while self.running:
            for e in self.win_messages:
                if not self.running:
                    return

                self.output(e['top'], 1)
                self.output(e['bottom'], 2)
                self.interrupt_event.wait(self.win_message_timeout)
                self.interrupt_event.clear()

    def transition(self, new_state: State):
        logger.info(f'Heart transitioning from {self.state} to {new_state}')

        self.state = new_state
        self.interrupt_event.set()
        self.location_broadcast.set_heart_state(new_state.value)


    def run_serial(self, serial: Serial):
        while self.running:
            self.interrupt_event.wait(self.serial_write_rate)

            if not serial.readable() or not serial.writable():
                continue # Serial not connected, retry

            if self.state == State.CONNECTED:
                serial.write(f'BRULIXHEARTCONNECTED{self.address}\r\n'.encode())
            else:
                serial.write(f'BRULIXHEART{self.address}\r\n'.encode())

            answer = serial.readline()
            if answer is None or len(answer) == 0:
                continue # No answer, retry

            try:
                answer = answer.decode().replace('\r\n', '')
            except UnicodeDecodeError:
                continue # Expected if the serial receives garbage

            if answer == f'BRULIXHEART{self.target}':
                logger.info(f'Received correct heart message')
                if self.state != State.CONNECTED:
                    self.transition(State.CONNECTED)
            elif answer == f'BRULIXHEARTCONNECTED{self.target}':
                logger.info(f'Received correct and connected heart message, leaving loop')
                if self.state != State.CONNECTED:
                    self.transition(State.CONNECTED)

                return
            else:
                logger.warning(f'Unexpected output from serial: {answer}')

            # Do nothing if the output doesn't match anything. The serial will output garbage if not plugged properly

    def run_connector(self):
        logger.info('Heart connector thread running')

        if self.serial_path is None:
            logger.warning('Serial path is null, leaving connector thread')
            return

        while self.running:
            try:
                with Serial(self.serial_path, baudrate=self.serial_speed, timeout=self.serial_timeout, inter_byte_timeout=self.serial_timeout, write_timeout=self.serial_timeout) as serial:
                    self.run_serial(serial)
                    break
            except:
                logger.error(f'Unexpected exception in heart connector thread, {traceback.format_exc()}')
                time.sleep(1)
                self.error_flag = True

        logger.info('Heart connector thread exiting')


    def run_impl(self):
        logger.info(f'Heart starting target={self.target}, rate={self.rate}, d={self.d}, k={self.k}')

        self.last_seq_number = None
        self.last_target_position = None
        lock = Lock()

        def on_location(device: int, sent_timestamp: datetime, seq_number: int, longitude: float, latitude: float, signal_strength: int, battery_level: int, heart_state: int):
            if device != self.target:
                return # Different device than the one we're tracking. Ignore

            if self.last_seq_number is not None and  seq_number <= self.last_seq_number:
                logger.warning(f'Received old position seq_number ({seq_number} <= {self.last_seq_number}), ignoring')
                return # Older packet than the one we have, ignore

            with lock:
                logger.debug(f'Received GPS position, seq_number={seq_number}')
                self.last_seq_number = seq_number
                self.last_target_position = (latitude, longitude)
                if self.state == State.RUNNING:
                    self.interrupt_event.set() # Update the BPM immediately when a GPS position is received

        last_position = None
        with self.location_receiver.register_callback(on_location):
            while self.running:
                if self.state == State.CONNECTING:
                    self.show_connecting_message()
                    continue
                elif self.state == State.CONNECTED:
                    if self.pwm is not None:
                        self.pwm.ChangeDutyCycle(0)

                    self.show_connected_message()
                    continue

                last_position = self.gps.get_position()

                time_left = ""
                if self.battery is not None:
                    try:
                        minutes_left = self.battery.get_remaining_time()
                        if minutes_left is not None:
                            time_left = f' | {round(minutes_left / 60)}:{(round(minutes_left % 60)):02d}'
                    except:
                        logger.error(f'Error while querying battery: {traceback.format_exc()}')
                        self.error_flag = True


                with lock:
                    bpm = '?'
                    distance = None

                    if self.forced_bpm is None and last_position is None:
                        bpm = ('WGPS')

                    elif self.forced_bpm is None and self.last_target_position is None:
                        bpm = 'WRGPS'

                    elif self.forced_bpm is None and self.last_target_position == (0, 0):
                        bpm = 'NRGPS'
                    else:
                        if self.forced_bpm:
                            bpm_value = self.forced_bpm
                        else:
                            distance = geopy.distance.geodesic(self.last_target_position, (last_position.latitude, last_position.longitude)).m
                            bpm_value = self.compute_bpm(distance)

                        bpm = f'{bpm_value} BPM'

                        if self.pwm is not None:
                            freq = (bpm_value * self.vibration_bpm_biais) / float(60)
                            if freq > 0:
                                self.pwm.ChangeFrequency(freq)

                            self.pwm.ChangeDutyCycle(freq * self.duty_cycle_ratio)

                if self.display_distance:
                    self.output(f'{round(distance) if distance is not None else "?"}m | {self.last_seq_number} seq', 2)
                else:
                    self.output(random.choice(self.messages), 2)

                self.output(f'{bpm}{time_left}')

                self.interrupt_event.wait(self.rate)
                self.interrupt_event.clear()

        if self.lcd is not None:
            self.lcd.clear()
            self.lcd.backlight(False)

        logger.info('Heart stopping')

    def force_bpm(self, bpm: int):
        logger.info(f'Forcing heart BPM to: {bpm}')
        self.forced_bpm = bpm
