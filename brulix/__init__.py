import click
import yaml
import traceback
import geopy.distance
import codecs
import time
import logging
import sys
import requests
import socket
import json
import signal
import os
from typing import Dict
from base64 import b64decode
from allora import Transport, EncryptionLayer, Rfm9x, BroadcastChannel, OrderedChannel
from allora.test.mock_device import MockDevice
from allora.packet import Parser, Builder
from .errors import *
from .location import *
from .chat import ChatHandler, MessagePacket
from .server import Server
from brulix import tasks

logger = logging.getLogger(__name__)

def send_emulated_packets(device: MockDevice, encryption: EncryptionLayer, packets: list):
    builder = Builder(encryption)

    for e in packets:
        if 'sleep' in e:
            time.sleep(e['sleep'])
        else:
            packet = builder.build(codecs.escape_decode(e['payload'])[0], e['source'], e['destination'], e['channel'], e.get('flags', 0), ttl=1)
            device.set_next_packet(packet)

    click.secho('Packet emulation completed', fg='yellow', bold=True)


def build_transport(config: dict, emulated: bool, emulated_packets: list, log_packets: bool) -> Transport:
    key = b64decode(config['network']['encryption']['key'])
    if len(key) != 16:
        raise ConfigurationError(f'Unexpected encryption key length: {len(key)} (expected 16 bytes)')

    encryption = EncryptionLayer(key)
    if emulated:
        click.secho('Using emulated driver', fg='yellow')
        parser = Parser(encryption)
        def send_callback(content: bytes):
            if log_packets:
                click.secho(f'Emulated device sent packet: {parser.parse(content)}')

        device = MockDevice()
        device.set_send_callback(send_callback)

        if emulated_packets:
            click.secho(f'Emulating {sum(1 for e in emulated_packets if "payload" in e)} packets', fg='yellow', bold=True)
            thread = Thread(target=send_emulated_packets, args=(device, encryption, emulated_packets))
            thread.daemon = True
            thread.start()
    else:
        import busio
        import board
        from digitalio import DigitalInOut
        CS = DigitalInOut(board.CE1)
        RESET = DigitalInOut(board.D25)
        spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
        device = Rfm9x(spi,
                       CS,
                       RESET,
                       5,
                       config['network']['device']['frequency'],
                       send_timeout=config['network']['device']['send_timeout'],
                       poll_rate=config['network']['device']['poll_rate'],
                       spreading_factor=config['network']['device']['spreading_factor'],
                       tx_power=config['network']['device']['tx_power'],
                       coding_rate=config['network']['device']['coding_rate'],
                       signal_bandwidth=config['network']['device']['signal_bandwidth'],
                       crc=config['network']['device']['crc'])

    return Transport(
            device,
            encryption,
            config['network']['address'],
            config['network']['transport']['receive_poll'],
            config['network']['transport']['random_factor'],
            relay_packets=config['network']['transport']['relay'],
            relay_random_factor=config['network']['transport']['relay_random_factor'],
            relay_wait=config['network']['transport']['relay_wait'],
            outgoing_ttl=config['network']['transport']['ttl'])

def configure_logging(config: dict):
    formatter = logging.Formatter(config['format'])

    handlers = {}
    def get_handler(level, file_handler: bool) -> logging.Logger:
        if (level, file_handler) not in handlers:
            handlers[(level, file_handler)] = logging.FileHandler(config['log_file'], 'a') if file_handler else logging.StreamHandler(sys.stderr)
            handlers[(level, file_handler)].setFormatter(formatter)
            handlers[(level, file_handler)].setLevel(level)

        return handlers[(level, file_handler)]

    levels = {'debug': logging.DEBUG, 'info': logging.INFO, 'warning': logging.WARNING, 'error': logging.ERROR}

    def setup_logger(file_handler: bool, handler_config: dict):
        for e in handler_config:
            local_logger = logging.getLogger(e) if e != "root" else logging.getLogger()
            level = levels.get(handler_config[e], None)
            if not level:
                raise ConfigurationError(f'Invalid logging level: {handler_config[e]}')

            local_logger.setLevel(logging.DEBUG) # Actually filtering is done at the handler level
            local_logger.addHandler(get_handler(level, file_handler))
            local_logger.propagate = False

    setup_logger(False, config['stderr'])
    setup_logger(True, config['file'])

class InteractiveTask:
    def wait(self, timeout: int):
        time.sleep(timeout)

    def interactive(self):
        return True

    def output(self, text: str, color=None, bold=False):
        click.secho(text, fg=color, bold=bold)

def run_remote_task(context, task: str):
    response = requests.post(f"http://{context.config['task']['remote_endpoint']}:{context.config['task']['remote_port']}/task/{task}")
    response.raise_for_status()

    port = response.json()['port']
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((context.config['task']['remote_endpoint'], port))

    ctrlc = False

    def on_ctrlc(*args):
        nonlocal ctrlc
        if ctrlc:
            click.secho(f'Ctrl-c pressed twice, exiting', fg='red', bold=True)
            sys.exit(1)

        ctrlc = True
        message = '{"type": "close"}'.encode()
        sock.send(len(message).to_bytes(2, 'big') + message)


    signal.signal(signal.SIGINT, on_ctrlc)

    while True:
        packet_size = sock.recv(2)
        if packet_size is None:
            break

        packet = sock.recv(int.from_bytes(packet_size, 'big'))
        if not packet:
            break

        content = json.loads(packet)
        click.secho(content['text'], fg=content['color'], bold=content['bold'])

    if not ctrlc:
        click.secho('Disconnected', fg='yellow', bold=True)

@click.group()
@click.option('--config', default='/etc/brulix/config.yml')
@click.option('--debug', default=False, is_flag=True)
@click.option('--emulated', default=False, is_flag=True)
@click.option('--emulated-packets', default=None)
@click.option('--log-packets', default=False, is_flag=True)
@click.option('--local', default=False, is_flag=True)
@click.pass_context
def cli(context, config: str, debug: bool, emulated: bool, emulated_packets: str, log_packets: bool, local: bool):
    class ctx:
        pass

    context.obj = ctx()
    context.obj.debug = debug
    context.obj.emulated = emulated
    context.obj.log_packets = log_packets
    context.obj.local = local
    context.obj.config_path = config

    with open(config) as fd:
        context.obj.config = yaml.safe_load(fd)

    if emulated_packets:
        with open(emulated_packets) as fd:
            context.obj.emulated_packets = yaml.safe_load(fd)
    else:
        context.obj.emulated_packets = None

    configure_logging(context.obj.config['logging'])

@cli.command()
@click.pass_obj
def listen(context):
    if context.local:
        transport = build_transport(context.config, context.emulated, context.emulated_packets, context.log_packets)
        with transport:
            tasks.listen(transport, InteractiveTask())
    else:
        run_remote_task(context, 'listen')

@cli.command()
@click.option('--rate', default='2')
@click.pass_obj
def rangetest(context, rate: str):
    try:
        if context.local:
            rate = float(rate)
            transport = build_transport(context.config, context.emulated, context.emulated_packets, context.log_packets)
            channel = BroadcastChannel(transport, context.config['location']['channel'])

            if 'emulated_position' in context.config['location']:
                emulated_position = context.config['location']['emulated_position']
                gps = EmulatedGPS(emulated_position['latitude'], emulated_position['longitude'])
            else:
                gps = GPSReader(context.config['location']['device'],
                                context.config['location']['baudrate'],
                                context.config['location']['ttl'],
                                context.config['location']['max_ttl_travel_distance'])

            location_database = LocationDatabase(context.config['location']['database'])

            with transport, channel, gps:
                def on_error(error):
                    print(f'Error reported: {error}')

                broadcast = LocationBroadcast(on_error, channel, context.config['location']['rate'], context.config['location']['random_factor'], gps, location_database, None)
                receiver = LocationReceiver(on_error, channel, location_database)

                with broadcast, receiver:
                    tasks.range_test_impl(broadcast, receiver, rate, gps, InteractiveTask())
        else:
            run_remote_task(context, 'rangetest')
    except:
        traceback.print_exc()
        if context.debug:
            import pdb
            pdb.post_mortem()
        else:
            raise

@cli.command()
@click.argument('user')
@click.pass_obj
def chat(context, user: str):
    try:
        if context.local:
            transport = build_transport(context.config, context.emulated, context.emulated_packets, context.log_packets)

            address = tasks.resolve_user(context.config['user_map'], user)
            if address == context.config['network']['address']:
                raise ConfigurationError('Self texting is not supported')


            config = context.config['network']['channel']
            channel = OrderedChannel(transport,
                                     context.config['chat']['channel'],
                                     address, config['connect_timeout'],
                                     config['ack_timeout'],
                                     config['disconnect_timeout'],
                                     config['connect_retry'],
                                     config['receive_poll'],
                                     config['random_factor'])

            with transport, channel:
                tasks.chat_impl(channel, user, InteractiveTask())
        else:
            run_remote_task(context, f'chat/{user}')
    except:
        traceback.print_exc()
        if context.debug:
            import pdb
            pdb.post_mortem()
        else:
            raise

@cli.command()
@click.argument('device', type=click.INT)
@click.argument('longitude', type=click.FLOAT)
@click.argument('latitude', type=click.FLOAT)
@click.option('--heart-state', '-h', type=click.INT, default=0)
@click.pass_obj
def writeposition(context, device: str, longitude: float, latitude: float, heart_state: int):
    database = LocationDatabase(context.config['location']['database'])

    last_entry = database.get_latest_position(device)
    seq = last_entry.seq_number + 1 if last_entry is not None else 1

    database.save_position(device, datetime.now(), seq, longitude, latitude, 0, heart_state)

    entry = database.get_latest_position(device)
    assert entry != last_entry
    assert entry.seq_number == seq

    click.secho(f'Created location entry: {entry}')


@cli.command
@click.option('--start', type=click.INT, default=868)
@click.option('--end', type=click.INT, default=920)
@click.option('--duration', type=click.INT, default=60)
@click.pass_obj
def scan(context, start: int, end: int, duration: int):
    click.secho(f'Scanning from {start}mhz to {end}mhz, duration={duration} seconds. Make sure no other instance is running', fg='yellow', bold=True)

    assert start < end


    transport = build_transport(context.config, context.emulated, context.emulated_packets, context.log_packets)


    results = []
    for i in range(start, end):
        previous_status = transport.status()

        if not context.emulated:
            transport.device.frequency_mhz = i
            transport.device.initialize()

        with transport:
            time.sleep(duration)

        status = transport.status()
        packets = status.received_packets - previous_status.received_packets
        corrupted_packets = status.corrupted_packets - previous_status.corrupted_packets

        results.append((i, packets, corrupted_packets))

        click.secho(f'{i}mhz: {packets} packets, {corrupted_packets} corrupted packets')

    click.secho('\nFinal results:', fg='green', bold=True)

    for frequency, packets, corrupted_packets,  in results:
        click.secho(f'- {frequency}mhz: {packets} packets, {corrupted_packets} corrupted packets')


@cli.command()
@click.pass_obj
@click.argument('import_path')
def importdb(context, import_path: str):
    database = LocationDatabase(context.config['location']['database'])

    click.secho(f'Imported {database.import_database(import_path)} entries')

@cli.command()
@click.pass_obj
def debug(context):
    click.secho(f'Building transport', fg='green')
    transport = build_transport(context.config, context.emulated, context.emulated_packets, context.log_packets)
    import pdb
    pdb.set_trace()

@cli.command()
@click.argument('bpm')
@click.pass_obj
def forcebpm(context, bpm):
    response = requests.post(f"http://{context.config['task']['remote_endpoint']}:{context.config['task']['remote_port']}/forcebpm", data=bpm)
    response.raise_for_status()

@cli.command()
@click.argument('debug')
@click.pass_obj
def debugheart(context, debug: str):
    if debug not in ['0', '1']:
        raise ValueError('Must be 0 or 1, not "{debug}"')

    response = requests.post(f"http://{context.config['task']['remote_endpoint']}:{context.config['task']['remote_port']}/debugheart", data=debug)
    response.raise_for_status()

@cli.command()
@click.pass_obj
def serve(context):
    transport = None
    transport_error = None

    try:
        transport = build_transport(context.config, context.emulated, context.emulated_packets, context.log_packets)
    except Exception as e:
        logger.error(f'Error while building transport: {traceback.format_exc()}')
        transport_error = e

    server = None

    def on_sigterm(self, *args):
        logger.info(f'Received sigint, stopping server')
        if server is not None and server.running:
            server.stop()

        os._exit(0)

    signal.signal(signal.SIGTERM, on_sigterm)

    try:
        server = Server(transport, context.config, context.config_path)

        if transport_error:
            server.add_error(transport_error)

        server.run()
    except:
        traceback.print_exc()
        logger.error(f'Error while serving: {traceback.format_exc()}')

        if context.config['pijuice']['enabled']:
            from pijuice import PiJuice
            PiJuice().config.SetLedConfiguration(context.config['pijuice']['status_led']['led'], {'function': 'USER_LED', 'parameter': context.config['pijuice']['status_led']['error_color']})


        if context.debug:
            import pdb
            pdb.post_mortem()

        if server is not None and server.running:
            server.stop()

        raise

def main():
    return cli()
