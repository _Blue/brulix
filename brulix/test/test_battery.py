from brulix.battery import *
from unittest import mock
import logging
import pytest

piJuiceMock = mock.MagicMock()

def error_routine(error):
    assert False

def test_raises_on_illegal_sample_size():
    with pytest.raises(ValueError):
        BatteryRecording(error_routine, 5, 1820, 0, piJuiceMock)

@mock.patch.object(logging.Logger, "warning")
def test_record_charge_level_error(mock):
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(return_value={'error' : 'ERROR', 'data' : '50'})

    recorder.record(1653790203.8867583)

    mock.assert_called()

@mock.patch.object(logging.Logger, "warning")
def test_record_io_voltage_error(mock):
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '50'})
    piJuiceMock.status.GetIoVoltage = mock.MagicMock(return_value={'error' : 'ERROR', 'data' : '5000'})

    recorder.record(1653790203.8867583)

    mock.assert_called()

@mock.patch.object(logging.Logger, "warning")
def test_record_io_current_error(mock):
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '50'})
    piJuiceMock.status.GetIoVoltage = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '5000'})
    piJuiceMock.status.GetIoCurrent = mock.MagicMock(return_value={'error' : 'ERROR', 'data' : '1000'})

    recorder.record(1653790203.8867583)

    mock.assert_called()

def test_record_first_battery_record():
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '50'})
    piJuiceMock.status.GetIoVoltage = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '5000'})
    piJuiceMock.status.GetIoCurrent = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '1000'})

    assert len(recorder.battery_records) == 0

    recorder.record(1653790203.8867583)

    assert len(recorder.battery_records) == 1
    assert recorder.battery_records[0].battery_percentage == 50

def test_get_remaining_time_return_none_when_not_enough_records():
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)

    assert recorder.get_remaining_time() is None

    recorder.battery_records.appendleft(BatteryRecord(1653790203.8867583, 50))

    assert recorder.get_remaining_time() is None

def test_get_remaining_time_difference_no_change():
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)

    recorder.battery_records.appendleft(BatteryRecord(1653790203.8867583, 50))
    recorder.battery_records.appendleft(BatteryRecord(1653790263.8867583, 50))

    assert recorder.get_remaining_time() is None

def test_get_remaining_time_difference_1():
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)

    recorder.battery_records.appendleft(BatteryRecord(1653790203.8867583, 50))
    recorder.battery_records.appendleft(BatteryRecord(1653790263.8867583, 49))
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '49'})

    assert recorder.get_remaining_time() == 49

def test_get_remaining_time_difference_5():
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)

    recorder.battery_records.appendleft(BatteryRecord(1653790203.8867583, 50))
    recorder.battery_records.appendleft(BatteryRecord(1653790263.8867583, 45))
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '45'})

    assert recorder.get_remaining_time() == 9

def test_get_remaining_time_difference_3_records():
    recorder = BatteryRecording(error_routine, 5, 1820, 5, piJuiceMock)

    recorder.battery_records.appendleft(BatteryRecord(1653790203.8867583, 50))
    recorder.battery_records.appendleft(BatteryRecord(1653790263.8867583, 49))
    recorder.battery_records.appendleft(BatteryRecord(1653790323.8867583, 45))
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '45'})

    assert recorder.get_remaining_time() == 18


def test_complete_story():
    recorder = BatteryRecording(error_routine, 60, 1820, 3, piJuiceMock)
    piJuiceMock.status.GetChargeLevel = mock.MagicMock(side_effect=[{'error' : 'NO_ERROR', 'data' : '50'}, {'error' : 'NO_ERROR', 'data' : '49'}, {'error' : 'NO_ERROR', 'data' : '49'},  {'error' : 'NO_ERROR', 'data' : '49'}, {'error' : 'NO_ERROR', 'data' : '49'}, {'error' : 'NO_ERROR', 'data' : '48'}, {'error' : 'NO_ERROR', 'data' : '48'},  {'error' : 'NO_ERROR', 'data' : '49'}, {'error' : 'NO_ERROR', 'data' : '49'}, {'error' : 'NO_ERROR', 'data' : '53'}, {'error' : 'NO_ERROR', 'data' : '53'}, {'error' : 'NO_ERROR', 'data' : '53'},  {'error' : 'NO_ERROR', 'data' : '53'}])
    piJuiceMock.status.GetIoVoltage = mock.MagicMock(side_effect=[{'error' : 'NO_ERROR', 'data' : '5000'}, {'error' : 'NO_ERROR', 'data' : '4000'}, {'error' : 'NO_ERROR', 'data' : '4000'}, {'error' : 'NO_ERROR', 'data' : '5000'}, {'error' : 'NO_ERROR', 'data' : '5000'}, {'error' : 'NO_ERROR', 'data' : '5000'}, {'error' : 'NO_ERROR', 'data' : '5000'}])
    piJuiceMock.status.GetIoCurrent = mock.MagicMock(side_effect=[{'error' : 'NO_ERROR', 'data' : '1000'}, {'error' : 'NO_ERROR', 'data' : '1000'}, {'error' : 'NO_ERROR', 'data' : '2000'}, {'error' : 'NO_ERROR', 'data' : '1000'}, {'error' : 'NO_ERROR', 'data' : '-1000'}, {'error' : 'NO_ERROR', 'data' : '-2000'}, {'error' : 'NO_ERROR', 'data' : '-2000'}])
    piJuiceMock.status.GetBatteryVoltage = mock.MagicMock(return_value={'error' : 'NO_ERROR', 'data' : '4000'})

    #Initial
    assert recorder.get_remaining_time() is None

    #1 - Battery: 50%. Output voltage: 5000V. Output current: 1000mA. Battery voltage: 4000V
    recorder.record(1653790203.8867583)

    assert recorder.get_remaining_time() is None

    #2 - Battery: 49%. Output voltage: 4000V. Output current: 1000mA. Battery voltage: 4000V
    recorder.record(1653790263.8867583)

    assert recorder.get_remaining_time() == 49

    #3 - Battery: 49%. Output voltage: 4000V. Output current: 2000mA. Battery voltage: 4000V
    recorder.record(1653790323.8867583)

    assert recorder.get_remaining_time() == 98

    #4 - Battery: 48%. Output voltage: 5000V. Output current: 1000mA. Battery voltage: 4000V
    recorder.record(1653790383.8867583)

    assert recorder.get_remaining_time() == 96

    #5 - Battery: 49%. Output voltage: 4000V. Output current: -1000mA. Battery voltage: 4000V
    recorder.record(1653790443.8867583)

    assert recorder.get_remaining_time() is None

    #6 - Battery: 53%. Output voltage: 4000V. Output current: -2000mA. Battery voltage: 4000V
    recorder.record(1653790503.8867583)
    assert recorder.get_remaining_time() == -21

    #7 - Battery: 53%. Output voltage: 4000V. Output current: -2000mA. Battery voltage: 4000V
    recorder.record(1653790563.8867583)

    assert recorder.get_remaining_time() == -26
