import json

from datetime import datetime
from threading import Event, Thread
from brulix.chat import ChatChannel, MessagePacket, MessageAckPacket, ChatPacketType
from brulix.xocial import Source, Conversation, Contact, Message, XocialWebsocketClient
from websocket_server import WebsocketServer
from queue import Queue


SOURCE = Source(display_name='test-source', type='Source', id=1)

ME_CONTACT = Contact(display_name='Me Contact', id=2, is_me=True)
CONTACT = Contact(display_name='Contact 1', id=3, is_me=False)
CONVERSATION = Conversation(SOURCE, display_name='Conversation 1', id=3, members=[CONTACT, ME_CONTACT])

TIMESTAMP = datetime.now().replace(microsecond=0)


TEST_PORT = 48950
TEST_ENDPOINT = f'ws://127.0.0.1:{TEST_PORT}'

class WebsocketTestServer:
    def __init__(self):
        self.thread = None
        self.loop = None
        self.connected_event = Event()

    def start(self):
        assert self.thread is None

        self.server = WebsocketServer(host='127.0.0.1', port=TEST_PORT)
        self.server.set_fn_message_received(self.on_client_message)
        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop(self):
        assert self.thread is not None

        self.server.shutdown_gracefully()
        self.thread.join()
        self.thread = None

    def __enter__(self, *args, **kwargs):
        self.start()
        return self

    def __exit__(self, *args, **kwargs):
        self.stop()

    def on_client_message(self, client, server, message):
        assert json.loads(message) == {'action': 'subscribe', 'types': ['Message'], 'compress': False}
        self.server.send_message(client, '{"result": "OK"}')
        self.connected_event.set()

    def run(self):
        self.server.run_forever()

    def wait_for_connect(self):
        self.connected_event.wait()
        self.connected_event.clear()

    def disconnect(self):
        self.queue.put(None)

    def send(self, message: str):
        self.server.send_message_to_all(message)

def make_client(callback):
    return XocialWebsocketClient(TEST_ENDPOINT, ['Message'], callback)

def test_websocket_basic_message():
    queue = Queue()

    with WebsocketTestServer() as server, make_client(queue.put) as client:
        server.wait_for_connect()

        message = Message('text', 12, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
        server.send(json.dumps(message.to_dict()))

        message = queue.get()

        assert message.text == 'text'
        assert message.id == 12
        assert message.author.id == ME_CONTACT.id
        assert message.conversation.id == CONVERSATION.id
        assert message.timestamp == TIMESTAMP.isoformat()
        assert message.remote_id is None


def test_websocket_basic_message_then_disconnect():
    queue = Queue()
    with WebsocketTestServer() as server, make_client(queue.put) as client:
        server.wait_for_connect()

        message = Message('text', 12, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
        server.send(json.dumps(message.to_dict()))

        received = queue.get()
        assert received.text == 'text'
        assert received.id == 12
        assert received.author.id == ME_CONTACT.id
        assert received.conversation.id == CONVERSATION.id
        assert received.timestamp == TIMESTAMP.isoformat()
        assert received.remote_id is None

        # Disconnect client
        server.stop()
        server.start()

        server.wait_for_connect()

        message_2 = Message('text-2', 13, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), message, TIMESTAMP.isoformat(), 15)
        server.send(json.dumps(message_2.to_dict()))

        received_2 = queue.get()
        assert received_2.text == 'text-2'
        assert received_2.id == 13
        assert received_2.author.id == ME_CONTACT.id
        assert received_2.conversation.id == CONVERSATION.id
        assert received_2.timestamp == TIMESTAMP.isoformat()
        assert received_2.reply_to == message
        assert received_2.remote_id == 15
        assert received_2.sent_timestamp == TIMESTAMP.isoformat()



