import random
from brulix.chat import ChatChannel, MessagePacket, MessageAckPacket, ChatPacketType
from brulix.xocial import Source, Conversation, Contact, Message, XocialClient
from brulix.test.mock_channel import MockChannel
from brulix.test.mock_xocial import MockXocialClient
from allora import Packet
from datetime import datetime
from threading import Event


SOURCE = Source(display_name='test-source', type='Source', id=1)

ME_CONTACT = Contact(display_name='Me Contact', id=2, is_me=True)
CONTACT = Contact(display_name='Contact 1', id=3, is_me=False)
CONVERSATION = Conversation(SOURCE, display_name='Conversation 1', id=3, members=[CONTACT, ME_CONTACT])

TIMESTAMP = datetime.now().replace(microsecond=0)


def make_test_channel(sent_callback, source=SOURCE, conversation=CONVERSATION, ack_timeout=10) -> ChatChannel:
    def error_routine(error):
        assert False

    return ChatChannel(source=source, conversation=conversation, client=MockXocialClient(), channel=MockChannel(sent_callback), ack_timeout=ack_timeout, error_routine=error_routine)

def test_chat_channel_simple_send_with_ack():
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

    message = Message('text', 12, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
    with make_test_channel(send_callback) as channel:

        channel.push_message(message)
        channel.sync()
        remote_id = 1212
        ack_packet = MessageAckPacket.build(remote_id, message.id)

        channel.client.inject_message(message)
        channel.client.expect_update_message(
                message.conversation,
                message.id,
                remote_id,
                1,
                None)

        channel.channel.receive(ack_packet.raw_content)

    assert len(packets) == 1
    parsed = MessagePacket(packets[0])
    assert parsed.sender_id == 12
    assert parsed.timestamp == TIMESTAMP
    assert parsed.reply_to_receiver_id is None
    assert parsed.text == 'text'


def test_chat_channel_simple_send_ack_timeout():

    ack_event = Event()
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

        if len(packets) >= 2:
            ack_event.set()

    message = Message('text', 12, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
    with make_test_channel(send_callback, ack_timeout=2) as channel:
        channel.push_message(message)
        channel.sync()
        remote_id = 1212
        ack_packet = MessageAckPacket.build(remote_id, message.id)

        channel.client.inject_message(message)
        channel.client.expect_update_message(
                message.conversation,
                message.id,
                remote_id,
                1,
                None)

        # Wait until a repeat is received to actually ack the packet
        ack_event.wait()
        channel.channel.receive(ack_packet.raw_content)

    assert len(packets) >= 2
    parsed = MessagePacket(packets[0])
    assert parsed.sender_id == 12
    assert parsed.timestamp == TIMESTAMP
    assert parsed.reply_to_receiver_id is None
    assert parsed.text == 'text'

    for e in packets:
        assert e == packets[0]

def test_chat_channel_simple_send_with_reply_to():
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

    message_1 = Message('text', 12, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
    remote_id_1 = 1212
    remote_id_2 = 1213

    with make_test_channel(send_callback) as channel:
        # Send first message
        channel.push_message(message_1)
        channel.sync()
        ack_packet_1 = MessageAckPacket.build(remote_id_1, message_1.id)

        channel.client.inject_message(message_1)
        channel.client.expect_update_message(
                message_1.conversation,
                message_1.id,
                remote_id_1,
                1,
                None)

        channel.channel.receive(ack_packet_1.raw_content)

        # Then send second
        message_1.remote_id = remote_id_1

        message_2 = Message('replied-text', 13, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), message_1, None, None)
        channel.push_message(message_2)
        channel.sync()

        ack_packet_2 = MessageAckPacket.build(remote_id_2, message_2.id)

        channel.client.inject_message(message_2)
        channel.client.expect_update_message(
                message_2.conversation,
                message_2.id,
                remote_id_2,
                1,
                message_1.id)

        channel.channel.receive(ack_packet_2.raw_content)


    assert len(packets) == 2
    parsed_1 = MessagePacket(packets[0])
    assert parsed_1.sender_id == 12
    assert parsed_1.timestamp == TIMESTAMP
    assert parsed_1.reply_to_receiver_id is None
    assert parsed_1.text == 'text'

    parsed_2 = MessagePacket(packets[1])
    assert parsed_2.sender_id == 13
    assert parsed_2.timestamp == TIMESTAMP
    assert parsed_2.text == 'replied-text'
    assert parsed_2.reply_to_receiver_id == 1212


def test_chat_channel_3_send_with_3_ack():
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

    message_1 = Message('text1', 12, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
    message_2 = Message('text2', 13, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
    message_3 = Message('text3', 14, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
    messages = [message_1, message_2, message_3]

    with make_test_channel(send_callback) as channel:
        channel.push_message(message_1)
        channel.push_message(message_2)
        channel.push_message(message_3)

        remote_ids = [1212, 1213, 1214]

        for message, remote_id in zip(messages, remote_ids):
            channel.wait_for_transaction()
            ack_packet = MessageAckPacket.build(remote_id, message.id)
            channel.client.inject_message(message)
            channel.client.expect_update_message(
                    message.conversation,
                    message.id,
                    remote_id,
                    1,
                    None)

            channel.channel.receive(ack_packet.raw_content)


    assert len(packets) == 3
    for message, packet in zip(messages, packets):
        parsed = MessagePacket(packet)

        assert parsed.sender_id == message.id
        assert parsed.timestamp == TIMESTAMP
        assert parsed.reply_to_receiver_id is None
        assert parsed.text == message.text



def test_chat_channel_simple_send_ack_twice():
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

    message = Message('text', 12, ME_CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
    with make_test_channel(send_callback) as channel:
        channel.push_message(message)
        channel.sync()
        assert len(packets) == 1
        parsed = MessagePacket(packets[0])

        assert parsed.sender_id == 12
        assert parsed.timestamp == TIMESTAMP
        assert parsed.reply_to_receiver_id is None
        assert parsed.text == 'text'


        remote_id = 1212
        ack_packet = MessageAckPacket.build(remote_id, message.id)

        channel.client.inject_message(message)
        channel.client.expect_update_message(
                message.conversation,
                message.id,
                remote_id,
                1,
                None)

        channel.channel.receive(ack_packet.raw_content)
        channel.channel.receive(ack_packet.raw_content) # Trigger another ack packet


def test_chat_channel_receive_message_with_ack():
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

    message = Message('text', 12, CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, TIMESTAMP.isoformat(), 1212)
    with make_test_channel(send_callback) as channel:
        message_packet = MessagePacket.build(TIMESTAMP, 'message-text', 1212, None)

        channel.client.expect_create_message(CONVERSATION,
                                             'message-text',
                                             1212,
                                             CONTACT.id,
                                             TIMESTAMP.timestamp(),
                                             datetime.now().timestamp(),
                                             None,
                                             message)

        channel.channel.receive(message_packet.raw_content)


    assert len(packets) == 1

    parsed = MessageAckPacket(packets[0])

    assert parsed.sender_id == 12
    assert parsed.receiver_id == 1212

def test_chat_channel_receive_already_created_message():
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

    message = Message('text', 12, CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, TIMESTAMP.isoformat(), 1212)
    with make_test_channel(send_callback) as channel:
        message_packet = MessagePacket.build(TIMESTAMP, 'message-text', 1212, None)

        channel.client.inject_message(message)
        channel.channel.receive(message_packet.raw_content)


    assert len(packets) == 1
    parsed = MessageAckPacket(packets[0])
    assert parsed.sender_id == 12
    assert parsed.receiver_id == 1212




def test_chat_channel_receive_message_with_reply_to_ack():
    packets = []
    def send_callback(packet: bytes):
        packets.append(packet)

    message_1 = Message('text-1', 12, CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, TIMESTAMP.isoformat(), 1212)
    message_2 = Message('text-2', 13, CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, TIMESTAMP.isoformat(), 1213)
    with make_test_channel(send_callback) as channel:
        message_packet_1 = MessagePacket.build(TIMESTAMP, 'text-1', 1212, None)
        message_packet_2 = MessagePacket.build(TIMESTAMP, 'text-2', 1213, 12)

        channel.client.expect_create_message(CONVERSATION,
                                             'text-1',
                                             1212,
                                             CONTACT.id,
                                             TIMESTAMP.timestamp(),
                                             datetime.now().timestamp(),
                                             None,
                                             message_1)

        channel.client.expect_create_message(CONVERSATION,
                                             'text-2',
                                             1213,
                                             CONTACT.id,
                                             TIMESTAMP.timestamp(),
                                             datetime.now().timestamp(),
                                             12,
                                             message_2)


        channel.channel.receive(message_packet_1.raw_content)
        channel.sync()

        channel.channel.receive(message_packet_2.raw_content)


    assert len(packets) == 2

    parsed_1 = MessageAckPacket(packets[0])
    assert parsed_1.sender_id == 12
    assert parsed_1.receiver_id == 1212


    parsed_2 = MessageAckPacket(packets[1])
    assert parsed_2.sender_id == 13
    assert parsed_2.receiver_id == 1213


def test_channel_send_receive_stress():
    expected_messages = []
    expected_messages_acks = []

    ack_queue = []
    message_packets = []
    ack_packets = []

    pending_ack = None
    message_sent_event = Event()
    def send_callback(packet: bytes):
        nonlocal pending_ack
        if packet[0] == ChatPacketType.Ack.value:
            ack_packets.append(MessageAckPacket(packet))
        else:
            message_packets.append(MessagePacket(packet))
            local_id =  message_packets[-1].sender_id

            assert pending_ack is None or pending_ack == local_id
            pending_ack = local_id
            message_sent_event.set()


    with make_test_channel(send_callback) as channel:
        for i in range(0, 250):
            action = random.choice([0, 1, 2])

            if action == 0: # Send a message
                message = Message(f'text-{i}', i, CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, None, None)
                message_with_remote_id = Message(f'text-{i}', i, CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, TIMESTAMP.isoformat(), i + 1000)
                expected_messages.append(message_with_remote_id)

                channel.client.inject_message(message)
                channel.client.expect_update_message(
                        message.conversation,
                        message_with_remote_id.id,
                        message_with_remote_id.remote_id,
                        1,
                        None)

                expected_messages.append(message)
                ack_queue.append(i)
                channel.push_message(message)

            elif action == 1: # Receive a message
                message = Message(f'text-{i}', i, CONTACT, CONVERSATION, TIMESTAMP.isoformat(), None, TIMESTAMP.isoformat(), i + 1000)

                channel.client.expect_create_message(CONVERSATION,
                                             message.text,
                                             message.remote_id,
                                             CONTACT.id,
                                             TIMESTAMP.timestamp(),
                                             datetime.now().timestamp(),
                                             None,
                                             message)

                expected_messages_acks.append(message)
                channel.channel.receive(MessagePacket.build(TIMESTAMP, f'text-{i}', message.remote_id, None).raw_content)

            else: # Send pending ack, if any
                if message_sent_event.is_set() and pending_ack is not None:
                    message_sent_event.clear()

                    local_id = ack_queue.pop(0)
                    assert local_id == pending_ack
                    ack_packet = MessageAckPacket.build(pending_ack + 1000, pending_ack)
                    pending_ack = None
                    channel.channel.receive(ack_packet.raw_content)

        # Complete transaction that are still pending
        while ack_queue:
            message_sent_event.wait()
            message_sent_event.clear()

            local_id = ack_queue.pop(0)
            ack_packet = MessageAckPacket.build(local_id + 1000, local_id)
            pending_ack = None
            channel.channel.receive(ack_packet.raw_content)


    # Validate that all expected packets were sent

    for message in expected_messages:
        assert any(e for e in message_packets if e.sender_id == message.id)

    for message in expected_messages_acks:
        assert any(e for e in ack_packets if e.sender_id == message.id)
