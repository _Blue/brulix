import os
import struct
from datetime import datetime, timedelta
from brulix.location import LocationEntry, LocationDatabase, LocationReceiver
from brulix.heart import State
from brulix.test.mock_channel import MockChannel
from threading import Thread


TEST_DATABASE_FILE = 'test.db'
TEST_DATABASE = f'sqlite:///{TEST_DATABASE_FILE}'
TIMESTAMP = datetime.now()

def make_database() -> LocationDatabase:
    try:
        os.remove(TEST_DATABASE_FILE)
    except FileNotFoundError:
        pass

    return LocationDatabase(TEST_DATABASE)

def make_receiver() -> LocationReceiver:
    def error_routine(e):
        assert e is None # To get a better error message
        assert False

    return LocationReceiver(error_routine, MockChannel(None), make_database())


def make_location_packet(seq_number: int, ts: int, latitude: float, longitude: float, battery_level=0, heart_state=None):
    packet = seq_number.to_bytes(8, 'big') + ts.to_bytes(8, 'big') + struct.pack('d', latitude) + struct.pack('d', longitude) + (battery_level or 0).to_bytes(1, 'big')
    if heart_state is not None:
        packet += heart_state.to_bytes(1, 'big')

    return packet


def test_location_no_position():
    database = make_database()
    assert database.get_latest_position(1) is None


def test_write_simple_location():
    database = make_database()

    database.save_position(1, TIMESTAMP, 12, 13.37, 37.13, 10, State.INVALID.value)
    entry = database.get_latest_position(1)

    assert entry.received_timestamp is not None
    assert entry.sent_timestamp == TIMESTAMP.timestamp()
    assert entry.seq_number == 12
    assert entry.longitude == 13.37
    assert entry.latitude == 37.13
    assert entry.battery_level == 10
    assert entry.heart_state == State.INVALID.value

    assert database.get_latest_position(2) is None

def test_write_multiple_locations():
    database = make_database()

    database.save_position(1, TIMESTAMP, 12, 13.37, 37.13, 3, State.INVALID.value)
    database.save_position(1, TIMESTAMP, 13, 13.38, 37.14, 2, State.INVALID.value)
    database.save_position(2, TIMESTAMP, 14, 13.35, 37.15, 1, State.INVALID.value)
    database.save_position(2, TIMESTAMP, 12, 13.36, 37.15, 0, State.INVALID.value)

    entry = database.get_latest_position(1)

    assert entry.received_timestamp is not None
    assert entry.sent_timestamp == TIMESTAMP.timestamp()
    assert entry.seq_number == 13
    assert entry.longitude == 13.38
    assert entry.latitude == 37.14
    assert entry.battery_level == 2
    assert entry.heart_state == State.INVALID.value

    entry_2 = database.get_latest_position(2)
    assert entry_2.received_timestamp is not None
    assert entry_2.sent_timestamp == TIMESTAMP.timestamp()
    assert entry_2.seq_number == 14
    assert entry_2.longitude == 13.35
    assert entry_2.latitude == 37.15
    assert entry_2.battery_level == 1
    assert entry_2.heart_state == State.INVALID.value

    assert database.get_latest_position(3) is None


def test_write_multiple_locations_stress():
    database = make_database()

    def run(device: int):
        for i in range(0, 100):
            database.save_position(device, TIMESTAMP, i, 13.37, 37.13, 4, State.CONNECTING.value)

    threads =[Thread(target=run, args=(i,)) for i in range(0, 4)]

    for e in threads:
        e.start()

    for e in threads:
        e.join()

    for i in range(0, 4):
        entry = database.get_latest_position(i)
        assert entry.received_timestamp is not None
        assert entry.sent_timestamp == TIMESTAMP.timestamp()
        assert entry.seq_number == 99
        assert entry.longitude == 13.37
        assert entry.latitude == 37.13
        assert entry.battery_level == 4
        assert entry.heart_state == State.CONNECTING.value


def test_query_location_non_null():
    database = make_database()

    database.save_position(1, TIMESTAMP, 12, 0, 0, 50, State.CONNECTED.value)
    assert database.get_latest_position(1, non_null=True) is None

    database.save_position(1, TIMESTAMP, 13, 1, 2, 51, State.CONNECTED.value)

    entry = database.get_latest_position(1, non_null=True)
    assert entry.received_timestamp is not None
    assert entry.sent_timestamp == TIMESTAMP.timestamp()
    assert entry.seq_number == 13
    assert entry.longitude == 1
    assert entry.latitude == 2
    assert entry.battery_level == 51
    assert entry.heart_state == State.CONNECTED.value


    database.save_position(1, TIMESTAMP, 14, 0, 0, 52, State.CREATED.value)
    entry = database.get_latest_position(1, non_null=True)
    assert entry.received_timestamp is not None
    assert entry.sent_timestamp == TIMESTAMP.timestamp()
    assert entry.seq_number == 13
    assert entry.longitude == 1
    assert entry.latitude == 2
    assert entry.battery_level == 51
    assert entry.heart_state == State.CONNECTED.value


    assert database.get_latest_position(1).seq_number == 14

def test_query_heart_state_none():
    database = make_database()

    assert database.get_heart_state(1) == (None, None)


def test_query_heart_state_connected():
    database = make_database()

    database.save_position(1, TIMESTAMP, 12, 0, 0, 50, State.CONNECTED.value)
    assert database.get_heart_state(1)[0] == State.CONNECTED.value

def test_query_heart_state_connected_with_transition():
    database = make_database()

    database.save_position(1, TIMESTAMP - timedelta(hours=1), 12, 0, 0, 50, State.CREATED.value)
    expected_ts = database.get_latest_position(1, non_null=False).received_timestamp

    database.save_position(1, TIMESTAMP, 13, 0, 0, 50, State.CONNECTED.value)
    state, ts = database.get_heart_state(1)

    assert state == State.CONNECTED.value
    assert ts == expected_ts


def test_query_heart_state_connected_with_multiple_transition():
    database = make_database()

    database.save_position(1, TIMESTAMP - timedelta(hours=1), 11, 0, 0, 50, State.CREATED.value)

    database.save_position(1, TIMESTAMP - timedelta(hours=1), 12, 0, 0, 50, State.CONNECTING.value)
    expected_ts = database.get_latest_position(1, non_null=False).received_timestamp
    database.save_position(1, TIMESTAMP, 13, 0, 0, 50, State.CONNECTED.value)
    state, ts = database.get_heart_state(1)

    assert state == State.CONNECTED.value
    assert ts == expected_ts

def test_query_heart_state_connected_with_multiple_final_states():
    database = make_database()

    database.save_position(1, TIMESTAMP - timedelta(hours=1), 11, 0, 0, 50, State.CREATED.value)
    expected_ts = database.get_latest_position(1, non_null=False).received_timestamp
    database.save_position(1, TIMESTAMP, 12, 0, 0, 50, State.CONNECTED.value)
    database.save_position(1, TIMESTAMP, 13, 0, 0, 50, State.CONNECTED.value)

    state, ts = database.get_heart_state(1)

    assert state == State.CONNECTED.value
    assert ts == expected_ts


def test_location_receiver_packet_stored():
    receiver = make_receiver()
    with receiver.channel, receiver:
        called = False
        def on_location(device: int, sent_timestamp: datetime, seq_number: int, longitude: float, latitude: float, signal_strength: int, battery_level: int, heart_state: int):
            nonlocal called
            called = True
            assert device == 1
            assert sent_timestamp == datetime.fromtimestamp(13)
            assert seq_number == 12
            assert latitude == 14.445
            assert longitude == 15.446
            assert heart_state is None
            assert battery_level == 0
            assert signal_strength is None

        with receiver.register_callback(on_location):
            packet = make_location_packet(12, 13, 14.445, 15.446)
            receiver.channel.receive(packet)


    latest = receiver.database.get_latest_position(1)
    assert latest.seq_number == 12
    assert latest.sent_timestamp == 13
    assert latest.latitude == 14.445
    assert latest.longitude == 15.446
    assert latest.heart_state is None
    assert latest.battery_level == 0
    assert called


def test_location_receiver_packet_stored_with_heart_state():
    receiver = make_receiver()
    with receiver.channel, receiver:
        called = False
        def on_location(device: int, sent_timestamp: datetime, seq_number: int, longitude: float, latitude: float, signal_strength: int, battery_level: int, heart_state: int):
            nonlocal called
            called = True
            assert device == 1
            assert sent_timestamp == datetime.fromtimestamp(13)
            assert seq_number == 12
            assert latitude == 14.445
            assert longitude == 15.446
            assert heart_state == 4
            assert battery_level == 50
            assert signal_strength is None

        with receiver.register_callback(on_location):
            packet = make_location_packet(12, 13, 14.445, 15.446, 50, 4)
            receiver.channel.receive(packet)


    latest = receiver.database.get_latest_position(1)
    assert latest.seq_number == 12
    assert latest.sent_timestamp == 13
    assert latest.latitude == 14.445
    assert latest.longitude == 15.446
    assert latest.heart_state == 4
    assert latest.battery_level == 50
    assert called


def test_location_receiver_packet_stored_twice():
    receiver = make_receiver()
    with receiver.channel, receiver:

        called = 0
        def on_location(device: int, sent_timestamp: datetime, seq_number: int, longitude: float, latitude: float, signal_strength: int, battery_level: int, heart_state: int):
            nonlocal called
            called += 1

            assert device == 1
            assert heart_state is None
            assert battery_level == 0
            assert signal_strength is None

            if called == 1:
                assert sent_timestamp == datetime.fromtimestamp(13)
                assert seq_number == 12
                assert latitude == 14.445
                assert longitude == 15.446
            else:
                assert called == 2
                assert seq_number == 13
                assert latitude == 14.446
                assert longitude == 15.447

        with receiver.register_callback(on_location):
            receiver.channel.receive(make_location_packet(12, 13, 14.445, 15.446))
            receiver.channel.receive(make_location_packet(13, 14, 14.446, 15.447))

    latest = receiver.database.get_latest_position(1)
    assert latest.seq_number == 13
    assert latest.sent_timestamp == 14
    assert latest.latitude == 14.446
    assert latest.longitude == 15.447
    assert latest.heart_state is None
    assert latest.battery_level == 0
    assert called == 2

def test_location_receiver_old_packet_ignored():
    receiver = make_receiver()
    with receiver.channel, receiver:

        called = 0
        def on_location(device: int, sent_timestamp: datetime, seq_number: int, longitude: float, latitude: float, signal_strength: int, battery_level: int, heart_state: int):
            nonlocal called
            called += 1

            assert device == 1
            assert heart_state is None
            assert battery_level == 0
            assert signal_strength is None

            assert sent_timestamp == datetime.fromtimestamp(14)
            assert seq_number == 13
            assert latitude == 14.446
            assert longitude == 15.447

        with receiver.register_callback(on_location):
            receiver.channel.receive(make_location_packet(13, 14, 14.446, 15.447))
            receiver.channel.receive(make_location_packet(12, 13, 14.445, 15.446))

    latest = receiver.database.get_latest_position(1)
    assert latest.seq_number == 13
    assert latest.sent_timestamp == 14
    assert latest.latitude == 14.446
    assert latest.longitude == 15.447
    assert latest.heart_state is None
    assert latest.battery_level == 0
    assert called == 1
