from allora import Packet
from allora.packet import DataPacket
from allora.test.test_channel import make_payload, make_binary_data_payload

class MockChannel:
    def __init__(self, send_callback, source=1, destination=2, channel_id=3):
        self.send_callback = send_callback
        self.running = False
        self.destination = destination
        self.source = source
        self.channel_id = channel_id

    def set_receive_callback(self, callback):
        self.receive_callback = callback

    def receive(self, content: bytes):
        assert self.running
        assert self.receive_callback is not None

        raw_packet = make_payload(make_binary_data_payload(content, 1), self.source, self.destination, self.channel_id, 0)
        self.receive_callback(DataPacket(Packet(raw_packet)))

    def start(self):
        assert not self.running
        self.running = True

    def stop(self):
        assert self.running
        self.running = False

    def __enter__(self):
        self.start()

    def __exit__(self, *args, **kwargs):
        if self.running:
            self.stop()

    def __del__(self):
        assert not self.running

    def send(self, packet: bytes):
        self.send_callback(packet)
