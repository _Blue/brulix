import time

from pytest import fail
from brulix.test.mock_channel import MockChannel
from brulix.chat import ChatService, MessageAckPacket, MessagePacket
from brulix.xocial import XocialClient, parse_timestamp
from threading import Event
from datetime import datetime

# Note: Xocial needs to actually run for these tests to pass
# (The CI ignores this file by default)

XOCIAL_ENDPOINT = 'http://127.0.0.1:8000/api'
XOCIAL_WEBSOCKET_ENDPOINT = 'ws://127.0.0.1:8001'
XOCIAL_SOURCE_NAME = f'test-source-{time.time()}'
XOCIAL_TIMEOUT = 10
ACK_TIMEOUT = 10


USER_MAP = {0: 'Myself', 1: 'User1', 2: 'User2'}
ADDRESS = 0

CLIENT = XocialClient(XOCIAL_ENDPOINT, XOCIAL_TIMEOUT)


def make_channel(callback):
    return MockChannel(callback)

class DummyRegisteredChannel:
    def __init__(self, channel):
        self.channel = channel

    def unregister(self):
        pass


def make_service(send_callback, ack_timeout=ACK_TIMEOUT) -> ChatService:
    def on_error(error):
        fail(f'Error reported {error}')

    return ChatService(on_error, CLIENT, XOCIAL_WEBSOCKET_ENDPOINT, XOCIAL_SOURCE_NAME, USER_MAP, ADDRESS, 20, ack_timeout, lambda _: DummyRegisteredChannel(make_channel(send_callback)))


def test_basic_source_configuration():
    with make_service(None) as service:
        service.sync()


    assert len(service.conversations) == 2

    for e in service.conversations:
        conversation = CLIENT.get_conversation(service.conversations[e].id)

        assert conversation is not None

        assert any(e.is_me for e in conversation.members)
        assert any(not e.is_me for e in conversation.members)
        assert len(conversation.members) == 2
        assert conversation.display_name in USER_MAP.values()


def test_send_message_with_ack():
    sent_event = Event()
    sent = []

    def on_send(packet: bytes):
        sent.append(packet)
        sent_event.set()


    with make_service(on_send) as service:
        service.sync() # Wait for initialization

        conversation = service.conversations[1]

        # Create a message in xocial
        sent_message = CLIENT.send_message(conversation, 'Test-Message-1', None)

        # Wait for the message to be picked up by the channel
        sent_event.wait()
        assert len(sent) == 1
        packet = MessagePacket(sent[0])

        assert packet.text == 'Test-Message-1'
        assert packet.sender_id == sent_message.id
        assert packet.timestamp.timestamp() == round(parse_timestamp(sent_message.timestamp).timestamp())

        channel = service.get_channel(conversation)

        remote_id = 1212
        ack = MessageAckPacket.build(remote_id, sent_message.id)
        channel.channel.receive(ack.raw_content)
        service.sync()


        message = CLIENT.get_message(sent_message.id)
        assert message.remote_id == 1212
        assert message.sent_timestamp is not None
        assert message.conversation.id == sent_message.conversation.id
        assert message.author.is_me
        assert message.timestamp == sent_message.timestamp

def test_send_message_ack_is_repeated():
    sent_event = Event()
    sent = []

    def on_send(packet: bytes):
        sent.append(packet)
        sent_event.set()


    with make_service(on_send, ack_timeout=1) as service:
        service.sync() # Wait for initialization

        conversation = service.conversations[1]

        # Create a message in xocial
        sent_message = CLIENT.send_message(conversation, 'Test-Message-6', None)

        # Wait for the message to be picked up by the channel
        sent_event.wait()
        sent_event.clear()
        assert len(sent) >= 1

        # Wait for ack to timeout
        sent_event.wait()

        assert len(sent) >= 2
        assert sent[0] == sent[1]

        packet = MessagePacket(sent[0])

        assert packet.text == 'Test-Message-6'
        assert packet.sender_id == sent_message.id
        assert packet.timestamp.timestamp() == round(parse_timestamp(sent_message.timestamp).timestamp())

        channel = service.get_channel(conversation)

        remote_id = 1216
        ack = MessageAckPacket.build(remote_id, sent_message.id)
        channel.channel.receive(ack.raw_content)
        service.sync()


        message = CLIENT.get_message(sent_message.id)
        assert message.remote_id == 1216
        assert message.sent_timestamp is not None
        assert message.conversation.id == sent_message.conversation.id
        assert message.author.is_me
        assert message.timestamp == sent_message.timestamp


def test_send_message_ack_twice():
    sent_event = Event()
    sent = []

    def on_send(packet: bytes):
        sent.append(packet)
        sent_event.set()


    with make_service(on_send) as service:
        service.sync() # Wait for initialization

        conversation = service.conversations[1]
        me_contact = service.me_contact

        # Create a message in xocial
        sent_message = CLIENT.send_message(conversation, 'Test-Message-1', None)

        # Wait for the message to be picked up by the channel
        sent_event.wait()
        assert len(sent) == 1
        packet = MessagePacket(sent[0])

        assert packet.text == 'Test-Message-1'
        assert packet.sender_id == sent_message.id
        assert packet.timestamp.timestamp() == round(parse_timestamp(sent_message.timestamp).timestamp())

        channel = service.get_channel(conversation)

        # Send the first ack
        remote_id = 1215
        ack = MessageAckPacket.build(remote_id, sent_message.id)
        channel.channel.receive(ack.raw_content)
        service.sync()

        message = CLIENT.get_message(sent_message.id)
        assert message.remote_id == 1215
        assert message.sent_timestamp is not None
        assert message.conversation.id == sent_message.conversation.id
        assert message.author.is_me
        assert message.timestamp == sent_message.timestamp

        # Send a second ack
        channel.channel.receive(ack.raw_content)
        service.sync()

        # Validate the message was not updated again
        message_after_second_ack = CLIENT.get_message(sent_message.id)
        assert message_after_second_ack == message


def test_receive_message_with_ack():
    sent_event = Event()
    sent = []

    def on_send(packet: bytes):
        sent.append(packet)
        sent_event.set()

    with make_service(on_send) as service:
        service.sync() # Wait for initialization

        conversation = service.conversations[1]

        # Send a message packet

        channel = service.get_channel(conversation)

        remote_id = 1213
        timestamp = datetime.now()
        packet = MessagePacket.build(timestamp, 'Test-Message-2', 1213, None)
        channel.channel.receive(packet.raw_content)

        # Wait for the message to be picked up by the channel
        sent_event.wait()

        assert len(sent) == 1
        ack = MessageAckPacket(sent[0])
        assert ack.receiver_id == remote_id

        # Validate that the message was correctly created in xocial
        message = CLIENT.get_message(ack.sender_id)
        assert message.remote_id == 1213
        assert message.sent_timestamp is not None
        assert message.conversation.id == conversation.id
        assert not message.author.is_me
        assert message.author.display_name == 'User1'

        assert round(parse_timestamp(message.timestamp).timestamp()) == round(timestamp.timestamp())


def test_receive_message_twice():
    sent_event = Event()
    sent = []

    def on_send(packet: bytes):
        sent.append(packet)
        sent_event.set()

    with make_service(on_send) as service:
        service.sync() # Wait for initialization

        conversation = service.conversations[1]

        # Send a message packet
        channel = service.get_channel(conversation)

        remote_id = 1217
        timestamp = datetime.now()
        packet = MessagePacket.build(timestamp, 'Test-Message-2', 1217, None)
        channel.channel.receive(packet.raw_content)

        # Wait for the message to be picked up by the channel
        sent_event.wait()
        sent_event.clear()

        assert len(sent) == 1
        ack = MessageAckPacket(sent[0])
        assert ack.receiver_id == remote_id

        # Validate that the message was correctly created in xocial
        message = CLIENT.get_message(ack.sender_id)
        assert message.remote_id == 1217
        assert message.sent_timestamp is not None
        assert message.conversation.id == conversation.id
        assert not message.author.is_me
        assert message.author.display_name == 'User1'

        assert round(parse_timestamp(message.timestamp).timestamp()) == round(timestamp.timestamp())

        # Resend the same message
        channel.channel.receive(packet.raw_content)

        # The exact same ack should be sent
        sent_event.wait()
        assert len(sent) == 2
        assert sent[0] == sent[1]

        channel.sync()
        message_after_second_sent = CLIENT.get_message(ack.sender_id)
        assert message_after_second_sent == message


def test_pre_existing_message_is_transmitted():
    sent_event = Event()
    sent = []

    def on_send(packet: bytes):
        sent.append(packet)
        sent_event.set()

    # Start the service to create the source and conversations
    with make_service(on_send) as service:
        service.sync()

    # Create a message while the service isn't running
    conversation = service.conversations[1]

    sent_message = CLIENT.send_message(conversation, 'Test-Message-3', None)

    # Validate that the message is picked up and transmitted once the service starts
    with service:
        sent_event.wait()
        assert len(sent) == 1
        packet = MessagePacket(sent[0])

        assert packet.text == 'Test-Message-3'
        assert packet.sender_id == sent_message.id
        assert packet.timestamp.timestamp() == round(parse_timestamp(sent_message.timestamp).timestamp())

        # Ack message
        channel = service.get_channel(conversation)
        remote_id = 1214
        ack = MessageAckPacket.build(remote_id, sent_message.id)
        channel.channel.receive(ack.raw_content)
        service.sync()


        message = CLIENT.get_message(sent_message.id)
        assert message.remote_id == 1214
        assert message.sent_timestamp is not None
        assert message.conversation.id == sent_message.conversation.id
        assert message.author.is_me
        assert message.timestamp == sent_message.timestamp



