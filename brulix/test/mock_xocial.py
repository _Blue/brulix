from brulix.xocial import Source, Message, Conversation, MessageNotFound

class MockXocialClient:
    def __init__(self):
        self.expected_create_messages = []
        self.expected_update_messages = []
        self.get_messages = []
        self.exited = False

    def __del__(self):
        assert self.exited

    def __enter__(self, *args, **kwargs):
        return self

    def __exit__(self, *args, **kwargs):
        assert self.expected_create_messages == []
        assert self.expected_update_messages == []

        self.exited = True

    def inject_message(self, message):
        self.get_messages.append(message)

    def expect_create_message(self,
            conversation: Conversation,
            text: str,
            remote_id: int,
            author_id: int,
            timestamp: int,
            received_timestamp: int,
            reply_to_id: int,
            output_message: Message):

        self.expected_create_messages.append((conversation, text, remote_id, author_id, timestamp, reply_to_id, output_message, received_timestamp))

    def expect_update_message(self,
            conversation: Conversation,
            local_id: int,
            remote_id: int,
            received_timestamp: int,
            reply_to_id: int):

        self.expected_update_messages.append((conversation, local_id, remote_id, reply_to_id, received_timestamp))

    def create_message(self,
            conversation: Conversation,
            text: str,
            remote_id: int,
            author_id: int,
            timestamp: int,
            received_timestamp: int,
            reply_to_id: int) -> Message:

        assert len(self.expected_create_messages) > 0
        assert self.expected_create_messages[0][:-2] == (conversation, text, remote_id, author_id, received_timestamp, reply_to_id)
        assert (timestamp is not None) == (self.expected_create_messages[0][4] is not None)
        message = self.expected_create_messages[0][6]
        self.expected_create_messages.pop(0)
        return message

    def update_message(self,
            conversation: Conversation,
            local_id: int,
            remote_id: int,
            received_timestamp: int,
            reply_to_id: int):

        assert len(self.expected_update_messages) > 0
        assert self.expected_update_messages[0][:-1] == (conversation, local_id, remote_id, reply_to_id)

        assert (received_timestamp is not None) == (self.expected_update_messages[0][4] is not None)

        self.expected_update_messages.pop(0)

        result = self.get_message(local_id)
        result.remote_id = remote_id

        return result

    def get_message(self, id: int) -> Message:
        message = next((e for e in self.get_messages if e.id == id), None)

        if message is None:
            raise MessageNotFound(f'Message {id} not found')

        return message

    def get_message_by_remote_id(self, conversation: Conversation, remote_id: int) -> Message:
        message = next((e for e in self.get_messages if e.remote_id == remote_id and e.conversation.id == conversation.id), None)
        if message is None:
            raise MessageNotFound()
        else:
            return message
