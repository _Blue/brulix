import logging
import json
import traceback
import dataclasses
import time
import socket
import select
import psutil
import shutil
import os
import sys
import yaml
from io import StringIO
from subprocess import check_call
from queue import Queue
from datetime import datetime
from allora import Transport, Channel, OrderedChannel, BroadcastChannel, BROADCAST_ADDRESS, Packet
from threading import Lock, Thread
from flask import Flask, request
from .errors import *
from .tasks import listen, range_test_impl, resolve_user, chat_impl
from .location import EmulatedGPS, GPSReader, LocationDatabase, LocationBroadcast, LocationReceiver, GPSStatus
from .chat import ChatService
from .xocial import XocialClient
from .battery import BatteryRecording
from .heart import Heart

CONFIG_BACKUP_FORMAT = '%Y-%m-%d--%H:%M:%S::%f'
MAX_ERROR_COUNT = 100

logger = logging.getLogger(__name__)
app = Flask(__name__)

server = None

class RegisteredChannel:
    def __init__(self, id: int, destination: int, server, channel: Channel):
        self.id = id
        self.destination = destination
        self.channel = channel
        self.server = server

    def __enter__(self):
        self.channel.__enter__()
        return self.channel

    def unregister(self):
        self.server.unregister_channel(self.id, self.destination)

    def __exit__(self, *args, **kwargs):
        self.channel.__exit__(*args, **kwargs)
        self.unregister()


class Server:
    def __init__(self, transport: Transport, config: dict, config_path: str):
        global server
        assert server is None
        server = self
        self.transport = transport
        self.config = config
        self.config_path = config_path
        self.running = False
        self.channels = {}
        self.lock = Lock()
        self.start_ts = None
        self.restart = False
        self.errors = []
        self.xocial_client = XocialClient(config['xocial']['api_endpoint'], config['xocial']['http_timeout'])
        self.location_database = LocationDatabase(config['location']['database'])
        self.location_broadcast = None
        self.location_receiver = None
        self.location_channel = None
        self.heart = None
        self.pijuice = None
        self.killswitch_sequence = config['network']['transport']['killswitch_sequence'].encode()
        self.packet_callback = None

        if config['pijuice']['enabled']:
            self.initialize_pijuice()
            self.set_status_led_color(config['pijuice']['status_led']['boot_color'])

        if config['battery']['enabled']:
            if self.pijuice is None:
                raise ConfigurationError('pijuice.enabled needs to be true for battery support')

            battery_profile = self.pijuice.config.GetBatteryProfile()
            if battery_profile['error'] != "NO_ERROR":
                logger.warning(f"Couldn't retrieve battery capacity from PiJuice ({battery_profile['error']}), defaulting to config value")
                battery_capacity = config['battery']['capacity']
            else:
                battery_capacity = battery_profile['data']['capacity']

            self.battery_recorder = BatteryRecording(self.add_error, config['battery']['poll_time'], battery_capacity, config['battery']['sample_size'], self.pijuice)
        else:
            logger.warning('Battery recording disabled via config.yml')
            self.battery_recorder = None


        if config['chat']['enabled']:
            self.chat_service = ChatService(self.add_error,
                                            self.xocial_client,
                                            config['xocial']['websocket_endpoint'],
                                            config['xocial']['source_name'],
                                            config['user_map'],
                                            config['network']['address'],
                                            config['xocial']['message_poll_rate'],
                                            config['chat']['ack_timeout'],
                                            self.create_chat_channel)
        else:
            logger.warning('ChatService disabled via config.yml')
            self.chat_service = None

        if 'emulated_position' in config['location']:
            emulated_position = config['location']['emulated_position']
            self.gps = EmulatedGPS(emulated_position['latitude'], emulated_position['longitude'])
        else:
            if config['location']['enabled']:
                self.gps = GPSReader(config['location']['device'],
                                     config['location']['baudrate'],
                                     config['location']['ttl'],
                                     config['location']['max_ttl_travel_distance'])
            else:
                self.gps = None

        self.location_broadcast, self.location_receiver, self.location_channel = self.initialize_location()

        if config['heart']['enabled']:
            from rpi_lcd import LCD

            lcd = None
            try:
                if config['heart']['lcd']:
                    lcd = LCD()
            except Exception as e:
                logger.error(f'Failed to load LCD screen, falling back to tty, {traceback.format_exc()}')
                self.add_error(e)

            self.heart = Heart(self.location_receiver,
                               self.location_broadcast,
                               config['network']['address'],
                               config['heart']['target'],
                               config['heart']['rate'],
                               config['heart']['display_distance'],
                               config['heart']['d'],
                               config['heart']['k'],
                               config['heart']['win_messages'],
                               config['heart']['win_message_timeout'],
                               config['heart']['connecting_message_timeout'],
                               config['heart']['serial'],
                               config['heart']['serial_speed'],
                               config['heart']['serial_write_rate'],
                               config['heart']['serial_timeout'],
                               config['heart']['vibration']['pin'],
                               config['heart']['vibration']['duty_cycle_ratio'],
                               config['heart']['vibration']['vibration_bpm_bias'],
                               config['heart']['messages'],
                               lcd,
                               self.gps,
                               self.battery_recorder)

    def start(self):
        with self.lock:
            assert not self.running
            logger.info('Starting server')

            self.start_ts = time.time()

            if self.battery_recorder is not None:
                self.battery_recorder.start()

            if self.transport is None:
                logger.error('Transport is null, starting the http endpoint only')

                self.set_status_led_color(self.config['pijuice']['status_led']['hardware_error_color'])
            else:
                self.transport.start()

                if self.gps is not None:
                    self.gps.start()

                if self.chat_service is not None:
                    self.chat_service.start()

                if self.location_channel is not None:
                    self.location_channel.channel.start()

                if self.location_broadcast is not None:
                    self.location_broadcast.start()

                if self.location_receiver is not None:
                    self.location_receiver.start()

                if self.heart is not None:
                    self.heart.start()

                self.set_status_led_color(self.config['pijuice']['status_led']['ok_color'])

                self.packet_callback = self.transport.register_packet_callback(self.on_packet)

            self.running = True

    def run(self):
        self.start()
        app.run(host=self.config['server']['host'], port=self.config['server']['port'])
        self.stop()

    def stop(self):
        try:
            with self.lock:
                assert self.running
                logger.info('Stopping server')
                self.running = False


            self.set_status_led_color({'r': 0, 'g': 0, 'b': 0})

            if self.battery_recorder is not None:
                self.battery_recorder.stop()

            if self.transport is not None:
                if self.heart is not None:
                    self.heart.stop()

                # Note: lock must be released for chat_service to unregister its channels without deadlocking
                if self.chat_service is not None:
                    self.chat_service.stop()

                if self.location_broadcast is not None:
                    self.location_broadcast.stop()

                if self.location_receiver is not None:
                    self.location_receiver.stop()

                # Note: location_channel is expected to be stopped in this block

                for e in self.channels:
                    if self.channels[e].channel.running:
                        self.channels[e].channel.stop()

                self.channels = []

                if self.gps is not None:
                    self.gps.stop()

            if self.packet_callback is not None:
                self.transport.unregister_packet_callback(self.packet_callback)
                self.packet_callback = None

            if self.transport is not None:
                self.transport.stop()
        except:
            logger.error(f'Error while stopping server: {traceback.format_exc()}')
        finally:
            if self.restart:
                logger.info('Re-execing')
                self.reexec()

    def on_packet(self, packet: Packet):
        if packet.payload == self.killswitch_sequence:
            logger.error(f'Received killswitch sequence from {packet.source}, shutting down')

            check_call('/sbin/poweroff')
        return False

    def initialize_pijuice(self):
        logger.info('Configuring pijuice')

        from pijuice import PiJuice
        self.pijuice = PiJuice()

        for button, config in self.config['pijuice']['buttons'].items():
            if button not in self.pijuice.config.buttons:
                logger.warning(f'PiJuice button "{button}" not supported by hardware, skipping')
            else:
                self.pijuice.config.SetButtonConfiguration(button, config)


    def set_status_led_color(self, color: dict):
        if not self.config['pijuice']['enabled']:
            return

        logger.info(f'Changing led color to {color}')

        self.pijuice.config.SetLedConfiguration(self.config['pijuice']['status_led']['led'], {'function': 'USER_LED', 'parameter': color})

    def initialize_location(self):
        channel = server.register_channel(self.config['location']['channel'],  BROADCAST_ADDRESS, lambda: BroadcastChannel(self.transport, self.config['location']['channel']))

        if self.config['location']['enabled']:
            location_broadcast = LocationBroadcast(self.add_error, channel.channel, self.config['location']['rate'], self.config['location']['random_factor'], self.gps, self.location_database, self.battery_recorder)
        else:
            location_broadcast = None

        location_receiver = LocationReceiver(self.add_error, channel.channel, self.location_database)

        return location_broadcast, location_receiver, channel

    def add_error(self, error: Exception):
        if len(self.errors) > MAX_ERROR_COUNT:
            logger.error(f'Max error count {MAX_ERROR_COUNT} reached, dropping {type(error)}')
            return

        self.errors.append((error, datetime.now()))

    def stop_from_request(self):
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Failed to find shutdown method')

        func()


    def reexec(self):
        logging.info('Restarting brulix')
        os.execvp(sys.argv[0], sys.argv)


    def register_channel(self, id: int, destination: int, routine) -> RegisteredChannel:
        with self.lock:
            key = (id, destination)
            if key in self.channels:
                raise ChannelInUseError(key)

            self.channels[key] = RegisteredChannel(id, destination, server, routine())
            logger.debug(f'Registered channel {key}')
            return self.channels[key]

    def unregister_channel(self, id: int, destination: int):
        with self.lock:
            key = (id, destination)
            if not key in self.channels:
                raise RuntimeError(f'Invalid state: channel {key} not registered')

            del self.channels[key]
            logger.debug(f'Unregistered channel {key}')


    def create_chat_channel(self, destination: int) -> RegisteredChannel:
        config = server.config['network']['channel']

        def build_channel():
            return OrderedChannel(self.transport,
                                  self.config['chat']['channel'],
                                  destination,
                                  None,
                                  config['ack_timeout'],
                                  config['disconnect_timeout'],
                                  config['connect_retry'],
                                  config['receive_poll'],
                                  config['random_factor'])

        # Note: the chat service isn't supposed to stop, so leaking a channel is ok
        # since the server will clean it up when it stops
        return server.register_channel(self.config['chat']['channel'], destination, build_channel)



class Task:
    def __init__(self, sock: socket, routine):
        self.socket = None
        self.listen_socket = sock
        self.routine = routine
        self.thread = None
        self.pending_inputs = Queue(100)

    def start(self):
        assert self.thread is None
        self.thread = Thread(target=self.run)
        self.thread.daemon = True
        self.thread.start()

    def wait(self, timeout: int):
        if not select.select([self.socket], [], [], timeout)[0]:
            return

        try:
            packet_size = self.socket.recv(2, socket.MSG_DONTWAIT)
            if not packet_size:
                raise InterruptedError() # Socket closed
        except BlockingIOError:
            return # Socket is still connected but has no available data

        msg = self.socket.recv(int.from_bytes(packet_size, 'big'))
        content = json.loads(msg)

        if content.get('type', None) == 'close':
            raise InterruptedError()
        elif content.get('type', None) == 'input':
            self.pending_inputs.put(content['input'])
        else:
            raise RuntimeError(f'Unexpected message: {content}')

    def input(self):
        self.send_message({'type': 'input'})
        # Wait until an input has been received
        while self.pending_inputs.empty():
            self.wait(0.05)

        return self.pending_inputs.get()

    def send_message(self, message: dict):
        content = json.dumps(message).encode()
        header = len(content).to_bytes(2, 'big')

        self.socket.send(header + content)


    def output(self, text: str, color=None, bold=False):
        if self.socket is None:
            return

        self.send_message({'text': text, 'color': color, 'bold': bold})

    def stop(self):
        if self.socket is not None:
            self.socket.close()

        if self.listen_socket is not None:
            self.listen_socket.close()

        logger.debug(f'Task completed')

    def interactive(self):
        return False

    def run(self):
        try:
            self.socket, address = self.listen_socket.accept()
            logger.debug(f'Task received connection from {address}')

            self.listen_socket.close()
            self.listen_socket = None
            self.routine(self)
        except InterruptedError:
            logger.debug('Task interrupted')
        except Exception:
            logger.error(f'Error during task: {traceback.format_exc()}')
            try:
                self.output(traceback.format_exc(), color='red', bold=True)
            except:
                logger.warning(f'Failed to write task errorn {traceback.format_exc()}')
        finally:
            self.stop()


def get_server() -> Server:
    global server
    if server is None:
        raise ServerNotRunningError()

    return server

def record_error(error: Exception):
    return get_server().add_error(error)


def start_task(server, routine) -> int:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((server.config['server']['host'], 0))
    sock.settimeout(server.config['task']['connect_timeout'])
    sock.listen(1)
    port = sock.getsockname()[1]

    task = Task(sock, routine)
    task.start()

    return port


@app.errorhandler(Exception)
def handle_exception(error: Exception):
    exception = traceback.format_exc()
    logger.error(f'Handling exception: {error}, stack trace: {exception}')

    content = exception.replace('\n', '\r\n')
    return content, (error.code if isinstance(error, HTTPException) else 500)


def get_temperature(sensor: str):
    with open(sensor) as fd:
        return float(fd.read()) / 1000


@dataclasses.dataclass
class BatteryStatus:
    battery_level: str
    running_on_internal_battery: bool
    temperature: str
    battery_minutes_remaining: int


def get_battery_status(pijuice, battery_recorder: BatteryRecording):
    charge_level = pijuice.status.GetChargeLevel()

    status = pijuice.status.GetStatus()

    if status['error'] == "NO_ERROR":
        #See: https://github.com/PiSupply/PiJuice/blob/master/Software/README.md#pijuice-settings
        usbPowerInput = status['data']['powerInput']
        gpioPowerInput = status['data']['powerInput5vIo']
        running_on_internal_battery = (usbPowerInput == "NOT_PRESENT" or usbPowerInput == "BAD") and (gpioPowerInput == "NOT_PRESENT" or gpioPowerInput == "BAD")

        temperature = pijuice.status.GetBatteryTemperature()
        temperature_value = temperature['data'] if (temperature['error'] == "NO_ERROR") else temperature['error']
    else:
        logger.error(f'Error while querying battery state, {status["error"]}')
        temperature_value = None
        running_on_internal_battery = None

    if charge_level['error'] == "NO_ERROR":
        battery_level_result = charge_level['data']
    else:
        battery_level_result = None

    return BatteryStatus(
        battery_level = battery_level_result,
        running_on_internal_battery = running_on_internal_battery,
        temperature = temperature_value,
        battery_minutes_remaining = battery_recorder.get_remaining_time(),
    )


@app.route('/status', methods=['GET'])
def status():
    server = get_server()

    @dataclasses.dataclass
    class Error:
        type: str
        details: str
        timestamp: float

    @dataclasses.dataclass
    class HeartStatus:
        state: int
        since: float

    @dataclasses.dataclass
    class Status:
        available_memory: int
        available_disk_space: int
        transport: Transport.Status
        running_since: float
        channels: list
        hostname: str
        temperature: float
        battery: BatteryStatus
        chat: ChatService.Status
        gps: GPSStatus
        errors: Error
        device_time: int
        heart_states: dict


    def channel_stats(channel: RegisteredChannel):
        values = channel.channel.statistics() .__dict__


        # Add channel id and destination
        values['id'] = channel.id
        values['destination'] = channel.destination
        if isinstance(channel.channel, OrderedChannel):
            values['state'] = channel.channel.state.name

            # Translate the timedela to seconds
            values['total_ack_wait'] = values['total_ack_wait'].seconds
            del values['current_ack_wait_start']

        return values

    def build_error(error: Exception, ts: datetime) -> Error:
        details = StringIO()
        traceback.print_exception(type(error), error, error.__traceback__, file=details)

        return Error(type=str(type(error).__name__), details=details.getvalue(), timestamp=ts.timestamp())

    def build_heart_status(state: int, since: float) -> HeartStatus:
        if state is None and since is None:
            return None
        else:
            return HeartStatus(state=state, since=round(since))


    battery_status = None if server.battery_recorder is None else get_battery_status(server.pijuice, server.battery_recorder)

    disk_stats = os.statvfs('/')
    status = Status(
            hostname = socket.gethostname(),
            available_memory = psutil.virtual_memory().available + psutil.swap_memory().free,
            available_disk_space = disk_stats.f_bavail / disk_stats.f_blocks,
            transport = server.transport.status() if server.transport is not None else None,
            running_since = server.start_ts,
            channels = [channel_stats(e) for e in server.channels.values()],
            temperature = get_temperature(server.config['temperature']['sensor']) if 'temperature' in server.config  and 'sensor' in server.config['temperature'] else None,
            battery = battery_status,
            chat = None if server.chat_service is None else server.chat_service.status(),
            gps = None if server.gps is None else server.gps.status(),
            errors = [build_error(*e) for e in server.errors],
            device_time = round(time.time()),
            heart_states = {server.config['heart_map'][e]: build_heart_status(*server.location_database.get_heart_state(e)) for e in server.config['heart_map']}
            )

    return app.response_class(response=json.dumps(dataclasses.asdict(status)),
                              mimetype='application/json')


@app.route('/users', methods=['GET'])
def users():
    server = get_server()

    return app.response_class(response=json.dumps(server.config['user_map']),
                              mimetype='application/json')


@app.route('/task/listen', methods=['POST'])
def listen_api():
    server = get_server()

    port = start_task(server, lambda task: listen(server.transport, task))

    return app.response_class(response=json.dumps({'port': port}),
                              mimetype='application/json')


def rangetest_wrapper(server: Server, task: Task):
    config = server.config

    if server.location_broadcast is not None:
        assert server.location_receiver is not None

        range_test_impl(server.location_broadcast, server.location_receiver, config['location']['range_test_rate'], server.gps, task)
    else:
        broadcast, receiver, registered_channel = server.initialize_location()

        with registered_channel:
            with broadcast, receiver:
                range_test_impl(broadcast, receiver, config['location']['range_test_rate'], server.gps, task)

@app.route('/task/rangetest', methods=['POST'])
def rangetest_api():
    server = get_server()

    port = start_task(server, lambda task: rangetest_wrapper(server, task))

    return app.response_class(response=json.dumps({'port': port}),
                              mimetype='application/json')


def chat_wrapper(server, task: Task, user: str):
    address = resolve_user(server.config['user_map'], user)
    if address == server.config['network']['address']:
        raise ConfigurationError('Self texting is not supported')

    config = server.config['network']['channel']

    def build_channel():
        return OrderedChannel(server.transport,
                              server.config['chat']['channel'],
                              address,
                              config['connect_timeout'],
                              config['ack_timeout'],
                              config['disconnect_timeout'],
                              config['connect_retry'],
                              config['receive_poll'],
                              config['random_factor'])

    with server.register_channel(server.config['chat']['channel'], address, build_channel) as channel:
        chat_impl(channel, user, task)

@app.route('/task/chat/<user>', methods=['POST'])
def chat(user: str):
    server = get_server()

    port = start_task(server, lambda task: chat_wrapper(server, task, user))
    return app.response_class(response=json.dumps({'port': port}),
                              mimetype='application/json')


@app.route('/config', methods=['GET', 'POST'])
def config_api():
    server = get_server()

    if request.method == 'GET':
        with open(server.config_path) as fd:
            raw_config = fd.read()

        parsed = yaml.safe_load(raw_config)
        match = parsed == server.config

        return app.response_class(response=json.dumps({'match': match, 'config': raw_config}),
                                  mimetype='application/json')

    else:
        assert request.method == 'POST'
        new_config_raw = request.data.decode()
        logger.info(f'Received new configuration {len(new_config_raw)} bytes')

        # Validate that config is parsable
        yaml.safe_load(new_config_raw)

        # Create config backup
        backup_path = f'{server.config_path}.{datetime.now().strftime(CONFIG_BACKUP_FORMAT)}'
        logger.info(f'Backing up config to {backup_path}')

        shutil.copyfile(server.config_path, backup_path)

        # Write new config
        with open(server.config_path, 'w') as fd:
            fd.write(new_config_raw)

        return app.response_class()

@app.route('/restart', methods=['POST'])
def restart_api():
    logger.info('Restarting server')

    server = get_server()
    server.restart = True

    try:
        server.stop_from_request()
    except:
        logger.error(f'Error while stopping server: {traceback.format_exc()}')

    return app.response_class()



@app.route('/shutdown', methods=['POST'])
def shutdown_api():
    logger.info('Shutting down machine')

    server = get_server()
    if server.pijuice is not None:
        logger.info('Shutting down pijuice in 60 secondes')
        server.pijuice.power.SetPowerOff(60)

    check_call('/sbin/poweroff')
    return app.response_class()

@app.route('/location', methods=['GET'])
def location_api():
    server = get_server()

    devices = list(server.config['user_map'].items()) + list(server.config['heart_map'].items())
    positions = {}

    for device, name in devices:
        position = server.location_database.get_latest_position(device, non_null=True)


        if position is None: # Better a null position than nothing at all
            positions[name] = server.location_database.get_latest_position(device, non_null=False)
        else: # Update the heart state in case the latest has a null location
            position.heart_state = server.location_database.get_latest_position(device, non_null=False).heart_state
            positions[name] = position

        if positions[name] is not None:
            positions[name].me = server.config['network']['address'] == positions[name].device

    entries = {device: dataclasses.asdict(position) if position else None for device, position in positions.items()}
    return app.response_class(response=json.dumps(entries),
                              mimetype='application/json')


@app.route('/time', methods=['POST'])
def set_time():
    ts = int(request.data.decode())

    logger.info('Setting system time to ts')
    check_call(['/bin/date', '-s', datetime.fromtimestamp(ts).isoformat()])

    return app.response_class()


@app.route('/forcebpm', methods=['POST'])
def force_bpm():
    server = get_server()
    if server.heart is None:
        return 'Heart is None', 409

    
    bpm = request.data.decode()
    if bpm.lower() in ['null', 'none']:
        forced_bpm = None
    else:
        try:
            forced_bpm = int(bpm)
        except ValueError:
            return 400, 'Invalid integer'

    server.heart.force_bpm(forced_bpm)
    return app.response_class()


@app.route('/debugheart', methods=['POST'])
def debug_heart():
    server = get_server()
    if server.heart is None:
        return 'Heart is None', 409


    debug = request.data.decode()
    if debug == '0':
        server.heart.display_distance = False
    elif debug == '1':
        server.heart.display_distance = True
    else:
        return 'Body must be 1 or 0', 400

    return app.response_class()
