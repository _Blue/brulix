import traceback
import time
import random
import struct
import pynmea2
import os
import geopy.distance
from serial import Serial
from allora import Channel, Packet
from threading import Thread, Event, Lock
from .errors import MalformedPacketError
from .battery import BatteryRecording
from datetime import datetime, timedelta
from dataclasses import dataclass
from decimal import Decimal
from sqlalchemy import create_engine, Column, Float, Integer, DateTime
from sqlalchemy.orm import declarative_base, Session
from logging import getLogger

Base = declarative_base()
logger = getLogger(__name__)


@dataclass
class GPSStatus:
    last_fix_ts: float
    last_error_ts: float
    current_latitude: float
    current_longitude: float
    time_for_first_fix: float
    emulated: bool
    errors: int
    thread_running: bool
    backoff: bool

class Position:
    def __init__(self, timestamp: datetime, latitude: float, longitude: float):
        self.timestamp = timestamp
        self.latitude = latitude
        self.longitude = longitude


    def __repr__(self) -> str:
        return f'Position({self.latitude},{self.longitude} [{self.timestamp}])'

class EmulatedGPS:
    def __init__(self, latitude: float, longitude: float):
        self.latitude = latitude
        self.longitude = longitude
        self.backoff = False

    def get_position(self) -> Position:
        return Position(datetime.now(), self.latitude, self.longitude)

    def __enter__(self, *args, **kwargs):
        pass

    def __exit__(self, *args, **kwargs):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def status(self) -> GPSStatus:
        return GPSStatus(
                None,
                None,
                self.latitude,
                self.longitude,
                None,
                emulated=True,
                errors=0,
                thread_running=False,
                backoff=False)


class GPSReader:
    def __init__(self, devices: list, baudrate: int, ttl: float, max_ttl_travel_distance: int):
        self.device_paths = devices
        self.baudrate = baudrate
        self.ttl = timedelta(seconds=ttl)
        self.max_ttl_travel_distance = max_ttl_travel_distance
        self.positions = []
        self.thread = None
        self.stop_event = Event()
        self.lock = Lock()
        self.start_ts = None
        self.first_fix_ts = None
        self.errors = 0
        self.last_error_ts = None
        self.backoff = False

    def start(self):
        assert self.thread is None
        assert not self.stop_event.is_set()

        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop(self):
        assert self.thread is not None
        assert not self.stop_event.is_set()

        self.stop_event.set()
        self.thread.join()
        self.thread = None
        self.stop_event.clear()

    def __enter__(self, *args, **kwargs):
        return self.start(*args, **kwargs)

    def __exit__(self, *args, **kwargs):
        if self.thread is not None:
            self.stop()

    def read_line(self, line: str):
        parsed = pynmea2.parse(line)

        if not isinstance(parsed, pynmea2.GGA):
            return # We only care about GGA messages

        # Check that the position is non-null
        if parsed.longitude == 0 and parsed.latitude == 0:
            return

        logger.debug(f'Received position from GPS: {parsed}')

        now = datetime.now()
        with self.lock:

            if self.positions:
                average = self.get_position_impl(self.positions)
                distance = geopy.distance.geodesic((average.latitude, average.longitude), (parsed.latitude, parsed.longitude)).m

                if distance > self.max_ttl_travel_distance:
                    logger.warning(f'Max distance exceeded ({distance}m) between ({parsed.latitude},{parsed.longitude}) and {average}, resetting location')
                    self.positions.clear()
                    return
            elif self.first_fix_ts is None:
                self.first_fix_ts = time.time()

            # Clear positions that are too old
            self.positions = [e for e in self.positions if e.timestamp + self.ttl > now]

            # Add position to list
            self.positions.append(Position(now, parsed.latitude, parsed.longitude))

    @staticmethod
    def get_position_impl(positions: list) -> Position:
        if not positions:
            return None

        # Average positions to return a more accurate result

        def average(values: list) -> float:
            sum = Decimal(0)
            for e in values:
                sum += Decimal(e)

            return float(sum / len(values))

        longitude = average([e.longitude for e in positions])
        latitude = average([e.latitude for e in positions])

        return Position(positions[-1].timestamp, latitude, longitude)

    def get_position(self) -> Position:
        with self.lock:
            positions = self.positions

        return GPSReader.get_position_impl(positions)

    def read_device(self, device: Serial):
        while not self.stop_event.is_set():
            self.read_line(device.readline().decode())
            self.backoff = False

    def run(self):
        logger.info(f'GPSReader starting. Devices={self.device_paths}, baudrate={self.baudrate}')

        self.start_ts = time.time()

        while not self.stop_event.is_set():
            try:
                selected_device = next((e for e in self.device_paths if os.path.exists(e)), None)
                if selected_device is not None:
                    with Serial(selected_device, baudrate=self.baudrate) as device:
                        self.read_device(device)
                else:
                    raise RuntimeError(f'No GPS found (devices: {self.device_paths}), waiting 10 seconds')
            except:
                logger.error(f'Unexpected exception while reading gps device: {selected_device}, {traceback.format_exc()}')
                self.errors += 1
                self.last_error_ts = time.time()
                self.backoff = True
                self.stop_event.wait(10) # Don't burn CPU if the device fails

    def status(self) -> GPSStatus:
        with self.lock:
            return GPSStatus(
                    self.positions[-1].timestamp.timestamp() if self.positions else None,
                    self.last_error_ts,
                    self.positions[-1].latitude if self.positions else None,
                    self.positions[-1].longitude if self.positions else None,
                    (self.first_fix_ts - self.start_ts) if self.first_fix_ts is not None else None,
                    emulated=False,
                    errors=self.errors,
                    thread_running=self.thread is not None and self.thread.is_alive(),
                    backoff=self.backoff)

class DatabaseLocationEntry(Base):
    __tablename__ = 'location_entry'

    id = Column(Integer, primary_key=True)
    received_timestamp = Column(DateTime)
    sent_timestamp = Column(DateTime)
    longitude = Column(Float)
    latitude = Column(Float)
    device = Column(Integer)
    seq_number = Column(Integer)
    battery_level = Column(Integer)
    heart_state = Column(Integer)

@dataclass
class LocationEntry:
    received_timestamp: int
    sent_timestamp: int
    longitude: float
    latitude: float
    device: int
    seq_number: int
    battery_level: int
    heart_state: int
    me: bool = False


class LocationDatabase:
    def __init__(self, database_string: str):
        logger.info(f'Opening location database: {database_string}')
        self.database = create_engine(database_string)
        Base.metadata.create_all(self.database)

        self.lock = Lock()

    def save_position(self, device: int, sent_timestamp: datetime, seq_number: int, longitude: float, latitude: float, battery_level: int, heart_state: int):
        with self.lock:
            entry = DatabaseLocationEntry(
                    received_timestamp=datetime.now(),
                    sent_timestamp=sent_timestamp,
                    longitude=longitude,
                    latitude=latitude,
                    device=device,
                    seq_number=seq_number,
                    battery_level=battery_level,
                    heart_state=heart_state)

            with self.make_session() as session:
                with session.begin():
                    session.add(entry)

    def get_latest_position(self, device: int, non_null=False) -> LocationEntry:
        with self.lock:
            with self.make_session() as session:
                with session.begin():
                    query = session.query(DatabaseLocationEntry).filter_by(device=device)

                    if non_null:
                        query = query.filter(DatabaseLocationEntry.longitude.isnot(0)).filter(DatabaseLocationEntry.latitude.isnot(0))

                    entries = query.order_by(-DatabaseLocationEntry.seq_number)
                    if any(entries):
                        entry = entries[0]
                        return LocationEntry(
                                received_timestamp=entry.received_timestamp.timestamp(),
                                sent_timestamp=entry.sent_timestamp.timestamp(),
                                device=entry.device,
                                seq_number=entry.seq_number,
                                longitude=entry.longitude,
                                latitude=entry.latitude,
                                battery_level=entry.battery_level,
                                heart_state=entry.heart_state)
                    else:
                        return None

    def get_heart_state(self, device: int):
        latest = self.get_latest_position(device, non_null=False)
        if latest is None:
            return None, None

        with self.lock:
            with self.make_session() as session:
                with session.begin():
                    query = session.query(DatabaseLocationEntry).filter(DatabaseLocationEntry.device.is_(device), DatabaseLocationEntry.heart_state.isnot(latest.heart_state))
                    entries = query.order_by(-DatabaseLocationEntry.seq_number)
                    if any(entries):
                        return latest.heart_state, entries[0].received_timestamp.timestamp()
                    else:
                        return latest.heart_state, latest.received_timestamp

    def make_session(self) -> Session:
        return Session(bind=self.database)

    def import_database(self, path: str) -> int:
        with self.lock:
            input_database = create_engine(f'sqlite:///{path}?mode=ro')

            imported = 0

            with self.make_session() as session:
                with session.begin():
                    def exists(entry: DatabaseLocationEntry) -> bool:
                        return any(session.query(DatabaseLocationEntry).filter_by(device=entry.device, seq_number=entry.seq_number))

                    with Session(bind=input_database) as input_session:
                        for entry in input_session.query(DatabaseLocationEntry):
                            if not exists(entry):

                                new_entry = DatabaseLocationEntry(
                                    received_timestamp=entry.received_timestamp,
                                    sent_timestamp=entry.sent_timestamp,
                                    longitude=entry.longitude,
                                    latitude=entry.latitude,
                                    device=entry.device,
                                    seq_number=entry.seq_number,
                                    battery_level=entry.battery_level,
                                    heart_state=entry.heart_state)

                                session.add(new_entry)

                                imported += 1

        return imported



class LocationPacket:
    def __init__(self, packet: Packet):
        if len(packet.payload) != 33 and len(packet.payload) != 34:
            raise MalformedPacketError(f'Unexpected length for Location packet: {len(packet.payload)} bytes')

        self.seq_number = packet.read_payload_int(0, 8)
        self.timestamp = datetime.fromtimestamp(packet.read_payload_int(8, 8))
        self.latitude = packet.read_payload_float(16, 8)
        self.longitude = packet.read_payload_float(24, 8)
        self.battery_level = packet.read_payload_int(32, 1)
        self.heart_state = None if len(packet.payload) < 34 else packet.read_payload_int(33, 1)
        self.packet = packet

    def __repr__(self) -> str:
        return f'LocationPacket(timestamp={self.timestamp}, latitude={self.latitude}, longitude={self.longitude} seq_number={self.seq_number}, battery={self.battery_level}, heat_state={self.heart_state})'


class RegisteredCallback:
    def __init__(self, routine, unregister_routine):
        self.routine = routine
        self.unregister_routine = unregister_routine

    def __enter__(self, *args, **kwargs):
        return self

    def __exit__(self, *args, **kwargs):
        self.unregister_routine(self)

    def call(self, device: int, sent_timestamp: datetime, seq_number: int, longitude: float, latitude: float, signal_strength: int, battery_level: int, heart_state: int):
        self.routine(device, sent_timestamp, seq_number, longitude, latitude, signal_strength, battery_level, heart_state)


class LocationBroadcast:
    def __init__(self, error_routine, channel: Channel, poll_time: float, random_factor: float, gps, database: LocationDatabase, battery: BatteryRecording):
        self.error_routine = error_routine
        self.channel = channel
        self.poll_time = poll_time
        self.random_factor = random_factor
        self.thread = None
        self.stop_event = Event()
        self.gps = gps
        self.battery = battery
        self.database = database
        self.callbacks = []
        self.lock = Lock()
        self.heart_state = None

    def start(self):
        assert self.thread is None

        self.stop_event.clear()
        self.thread = Thread(target=self.run)
        self.thread.start()

    def stop(self):
        assert self.thread is not None
        assert not self.stop_event.is_set()

        self.stop_event.set()
        self.thread.join()

        self.thread = None

    def __enter__(self, *args, **kwargs):
        return self.start(*args, **kwargs)

    def __exit__(self, *args, **kwargs):
        if self.thread is not None:
            self.stop()

    def run(self):
        last_position = self.database.get_latest_position(self.channel.transport.address)
        seq_number = last_position.seq_number + 1 if last_position is not None else 0
        logger.info(f'LocationBroadcast starting, initial seq_number={seq_number}')

        while not self.stop_event.wait(self.poll_time + random.uniform(0, self.random_factor)):
            try:
                self.broadcast(seq_number)

                seq_number += 1
            except Exception as e:
                logger.error(f'Error while broadcasting location, {traceback.format_exc()}')
                self.error_routine(e)

        logger.info('LocationBroadcast stopping')

    def set_heart_state(self, state):
        logger.info(f'Setting heart state to {state}')
        self.heart_state = state

    def broadcast(self, seq_number: int):
        position = self.gps.get_position()
        if position is None: # TODO: Maybe don't broadcast if we don't have a fix ?
            latitude = 0
            longitude = 0
        else:
            latitude = position.latitude
            longitude = position.longitude

        battery_level = None
        if self.battery is not None:
            try:
                battery_level = self.battery.get_battery_charge_percentage()
                if battery_level is not None and battery_level < 0:
                    battery_level = 255
            except:
                logger.error(f'Failed to get battery level, {traceback.format_exc()}')

        ts = round(time.time())
        dt_ts = datetime.fromtimestamp(ts)
        self.database.save_position(self.channel.transport.address, dt_ts, seq_number, longitude, latitude, battery_level, self.heart_state or 0)
        logger.debug(f'Broadcasting location: {latitude}, {longitude} (seq_number={seq_number})')

        packet = seq_number.to_bytes(8, 'big') + ts.to_bytes(8, 'big') + struct.pack('d', latitude) + struct.pack('d', longitude) + (battery_level or 0).to_bytes(1, 'big')
        if self.heart_state is not None:
            packet += self.heart_state.to_bytes(1, 'big')

        self.channel.send(packet)

        with self.lock:
            for e in self.callbacks:
                e.call(self.channel.transport.address, dt_ts, seq_number, longitude, latitude, None)

    def register_callback(self, callback) -> RegisteredCallback:
        registered_callback = RegisteredCallback(callback, self.unregister_callback)

        with self.lock:
            self.callbacks.append(registered_callback)

        return registered_callback

    def unregister_callback(self, registered_callback: RegisteredCallback):
        with self.lock:
            previous_size = len(self.callbacks)
            self.callbacks = [e for e in self.callbacks if not e is registered_callback]

            if previous_size != len(self.callbacks) - 1:
                logger.error(f'Unexpected diff when unregistering LocationBroadcast callback, previous size={previous_size}, new_size={len(self.callbacks)}')

    def push_poll_time(self, poll_time: float):
        logger.info(f'LocationBroadcast pushing poll time to {poll_time}')

        self.stop()

        revert_poll_time = self.poll_time
        self.poll_time = poll_time

        self.start()

        class RevertPollTimeRAII:
            def __init__(self, instance):
                self.instance = instance

            def __enter__(self, *args, **kwargs):
                return self

            def __exit__(self, *args, **kwargs):
                logger.info(f'LocationBroadcast reverting poll time to {revert_poll_time}')

                self.instance.stop()
                self.instance.poll_time = revert_poll_time
                self.instance.start()

        return RevertPollTimeRAII(self)


class LocationReceiver:
    def __init__(self, error_routine, channel: Channel, database: LocationDatabase):
        self.error_routine = error_routine
        self.channel = channel
        self.callbacks = []
        self.lock = Lock()
        self.running = False
        self.database = database

    def register_callback(self, callback) -> RegisteredCallback:
        registered_callback = RegisteredCallback(callback, self.unregister_callback)

        with self.lock:
            self.callbacks.append(registered_callback)

        return registered_callback

    def unregister_callback(self, registered_callback: RegisteredCallback):
        with self.lock:
            previous_size = len(self.callbacks)
            self.callbacks = [e for e in self.callbacks if not e is registered_callback]

            if previous_size != len(self.callbacks) + 1:
                logger.error(f'Unexpected diff when unregistering LocationReceiver callback, previous size={previous_size}, new_size={len(self.callbacks)}')

    def on_packet(self, packet: Packet):
        try:
            with self.lock:
                if not self.running:
                    return

                parsed = LocationPacket(packet)

                # Make sure that we don't consider an older location packet
                latest = self.database.get_latest_position(packet.source)

                if latest is not None and latest.seq_number >= parsed.seq_number:
                    logger.warning(f'Received location packet with a seq number <= than the latest one in db ({latest.seq_number}), dropping: {parsed}')
                    return

                logger.debug(f'Received location packet from {packet.source}: {parsed}')
                self.database.save_position(packet.source, parsed.timestamp, parsed.seq_number, parsed.longitude, parsed.latitude, parsed.battery_level, parsed.heart_state)

                for e in self.callbacks:
                    e.call(packet.source, parsed.timestamp, parsed.seq_number, parsed.longitude, parsed.latitude, packet.signal_strength, parsed.battery_level, parsed.heart_state)

        except Exception as e:
            logger.error(f'Unexpected error while parsing location packet, {traceback.format_exc()}')
            self.error_routine(e)

    def start(self):
        assert not self.running

        logger.info('LocationReceiver starting')
        self.running = True
        self.channel.set_receive_callback(self.on_packet)

    def stop(self):
        assert self.running

        logger.info('LocationReceiver stopping')
        self.running = False

        with self.lock:
            self.channel.set_receive_callback(self.on_packet)

    def __enter__(self, *args, **kwargs):
        return self.start(*args, **kwargs)

    def __exit__(self, *args, **kwargs):
        if self.running:
            self.stop()

