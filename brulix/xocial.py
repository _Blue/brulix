import requests
from typing import Optional, List
from logging import getLogger
import json
import urllib.parse
import websocket
import traceback
import time
from datetime import datetime
from dataclasses import dataclass
from dataclasses_json import dataclass_json
from threading import Thread, Lock, Event

logger = getLogger(__name__)

@dataclass_json
@dataclass
class Source:
    display_name: str
    type: str
    id: int

@dataclass_json
@dataclass
class SourceContact:
    display_name: str
    id: int
    is_me: bool
    display_image: Optional[str] = None

@dataclass_json
@dataclass
class Contact:
    display_name: str
    id: int
    is_me: bool
    display_image: Optional[str] = None

@dataclass_json
@dataclass
class Conversation:
    source: Source
    display_name: str
    id: int
    members: List[Contact]
    display_image: Optional[str] = None

@dataclass_json
@dataclass
class Message:
    text: str
    id: int
    author: Contact
    conversation: Conversation
    timestamp: str
    reply_to: Optional['Message'] = None
    sent_timestamp: Optional[str] = None
    remote_id: Optional[int] = None


class MessageNotFound(RuntimeError):
    pass


def parse_timestamp(time_string: str) -> datetime:
    return datetime.fromisoformat(time_string.replace('Z', ''))


class XocialWebsocketClient:
    def __init__(self, endpoint, types: list, callback):
        self.endpoint = endpoint
        self.types = types
        self.callback = callback
        self.running = False
        self.thread = None
        self.connected = False
        self.connection = None
        self.connection_lock = Lock()
        self.connected_event = Event()

    def start(self):
        assert not self.running
        assert self.thread is None

        logger.info(f'Websocket client starting')
        self.running = True
        self.thread = Thread(target=self.run)
        self.thread.start()

    def wait_for_connection(self, interrupt_event: Event):
        while not interrupt_event.is_set():
            if self.connected_event.wait(0.5):
                return True

        return False

    def stop(self):
        assert self.running

        logger.info(f'Websocket client stopping')
        self.running = False

        with self.connection_lock:
            if self.connection:
                self.connection.close()
                self.connection = None

        self.thread.join()
        self.thread = None

        self.connected_event.clear()

        logger.info(f'Websocket client stopped')

    def __enter__(self, *args, **kwargs):
        self.start()
        return self

    def __exit__(self, *args, **kwargs):
        if self.running:
            self.stop()

    def process_message(self, message: str):
        if not message:
            raise websocket.WebSocketConnectionClosedException('Connection closed')
            # Can happen if the socket has been closed

        message = Message.from_dict(json.loads(message))
        self.callback(message)

    def run_impl(self):
        try:
            logger.info(f'Connecting to websocket: {self.endpoint}')

            with self.connection_lock:
                if not self.running:
                    return

                self.connection = websocket.create_connection(self.endpoint)
                logger.info(f'Websocket connected')
                self.connected = True

            self.connection.send(json.dumps({'action': 'subscribe', 'types': ['Message'], 'compress': False}))
            reply = json.loads(self.connection.recv())

            self.connected_event.set()

            if not reply['result'] == 'OK':
                raise RuntimeError(f'Unexpected reply in websocket: {reply}')

            while self.running:
                self.process_message(self.connection.recv())
        except websocket.WebSocketConnectionClosedException:
            if self.running:
                logger.error(f'Socket closed while websocket is running, {traceback.format_exc()}')
        except:
            self.connected = False
            logger.error(f'Error in websocket: {traceback.format_exc()}')
            time.sleep(1) # Don't burn CPU

            with self.connection_lock:
                if self.connection:
                    self.connection.close()
                    self.connection = None

    def run(self):
        while self.running:
            self.run_impl()

class XocialClient:
    def __init__(self, endpoint: str, timeout: int):
        self.endpoint = endpoint
        self.timeout = timeout

    def get(self, uri: str) -> dict:
        response = requests.get(f'{self.endpoint}/{uri}', timeout=self.timeout)
        response.raise_for_status()

        return response.json()

    def post(self, uri: str, body: dict) -> dict:
        headers = {'Content-Type': 'application/json'}
        response = requests.post(f'{self.endpoint}/{uri}', timeout=self.timeout, data=json.dumps(body))
        response.raise_for_status()

        return response.json()

    def get_or_create_source(self, name: str):
        sources = [Source.from_dict(e) for e in self.get('source')]

        matched_source = next((e for e in sources if e.display_name == name), None)
        if matched_source is not None:
            return matched_source

        logger.info(f'No source matching {name}, creating one')
        return Source.from_dict(self.post('source', {'type': 'SyntheticSource', 'display_name': name}))

    def get_or_create_source_contact(self, source: Source, display_name: str, is_me: bool) -> SourceContact:
        return SourceContact.from_dict(self.post(f'source/{source.id}/sourcecontact', {'display_name': display_name, 'is_me': is_me}))

    def get_or_create_contact(self, source: Source, source_contact: SourceContact) -> Contact:
        return SourceContact.from_dict(self.post(f'source/{source.id}/contact', {'source_contact_id': source_contact.id}))

    def get_or_create_conversation(self, source: Source, display_name: str, contacts: list) -> Conversation:
        return Conversation.from_dict(self.post(f'source/{source.id}/conversation', {'display_name': display_name, 'display_image': None, 'members': [e.id for e in contacts]}))

    def search(self, conversation: Conversation, author: Contact, start_index: int, count: int):
        args = {'count': count, 'author': author.id}
        if start_index is not None:
            args['start_index'] = start_index

        return Message.from_dict(self.get(f'conversation/{conversation.id}/message/search' + urllib.parse.urlencode(args)))

    def get_unsent_messages(self, source: Source) -> list:
        args = {'custom_query': {'conversation__source_id': source.id, 'remote_id__isnull': True}, 'order': 'asc'}

        result = self.post('synthetic_message', args)
        return [Message.from_dict(e) for e in result]

    def get_message(self, id: int) -> Message:
        return Message.from_dict(self.get(f'message/{id}'))

    def get_conversation(self, id: int) -> Conversation:
        return Conversation.from_dict(self.get(f'conversation/{id}'))


    def send_message(self, conversation: Conversation, text: str, reply_to_id: int) -> Message:
        body = {'text': text}

        if reply_to_id is not None:
            body['reply_to'] = reply_to_id

        return Message.from_dict(self.post(f'conversation/{conversation.id}/message', body))

    def create_message(
            self,
            conversation: Conversation,
            text: str,
            remote_id: int,
            author_id: int,
            timestamp: int,
            received_timestamp: int,
            reply_to_id: int) -> Message:

        body = {
                'text': text,
                'remote_id': remote_id,
                'author': author_id,
                'timestamp': timestamp,
                'sent_timestamp': received_timestamp
                }

        if reply_to_id is not None:
            body['reply_to'] = reply_to_id

        return Message.from_dict(self.post(f'source/{conversation.source.id}/{conversation.id}/', body))

    def update_message(
            self,
            conversation: Conversation,
            local_id: int,
            remote_id: int,
            sent_timestamp: int,
            reply_to_id: int
            ) -> Message:

        body = {
                'remote_id': remote_id,
                'sent_timestamp': sent_timestamp
               }

        if reply_to_id is not None:
            body['reply_to'] = reply_to_id

        return Message.from_dict(self.post(f'source/{conversation.source.id}/{conversation.id}/{local_id}', body))


    def get_message_by_remote_id(self, conversation: Conversation, remote_id: int) -> Message:
        entries = self.post('synthetic_message', {'custom_query': {'conversation_id': conversation.id, 'remote_id': remote_id}})

        if len(entries) > 1:
            raise RuntimeError(f'Multiple messages returned by xocial for remote_id={remote_id}, conversation_id={conversation.id}')
        elif len(entries) == 0:
            raise MessageNotFound()
        else:
            return Message.from_dict(entries[0])
